# Battlefield Demo
This sample shows the beginning stages of how players will move across the map. It will progress in various stages.

# Current Stage
A player can move the blue Roric units across the map. The Red Blades units will move to the closest neutral resource (tiles with brown treasure chests).

# Controls
* Use the arrow key to move the cursor across the tiles.
* Press Z to select a unit and menu options. You can only move for now.
* Press X to cancel the last selected command
* Press I to see information about a tile

# Maps
Use the drop down menu in the top right control panel to select a map created with the mapeditor to load.  Click the Random button to load a 
randomly generated one. Clicking either button will reload the page.

# Known issues
* Right now, AI units are being assigned destinations in the order in which units are listed, but it
  should really be based on which tile is closest to that unit.
* AI units may not move if another unit is standing at their final destination

# Note
Hover the mouse over a tile to see its coordinates and tile number in the upper right developer panel. When the enemy units move, their destination
tile number is displayed in the browser console.

Programmers will want to see the [documentation]

This demo is meant to run out of _siteroot_/game/battlefield, where _siteroot_ is the root of your site. For the map loading to work properly, the mapeditor files must be at _siteroot_/game/mapeditor.

For easy viewing of this README, install [Markdown viewer for Firefox].

[Markdown viewer for Firefox]: https://addons.mozilla.org/firefox/downloads/file/204110/markdown_viewer-1.3-fx.xpi?src=dp-btn-primary
[documentation]: http://aninternetpresence.net/game/docs/battlefield
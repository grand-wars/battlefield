window.addEventListener("load", function(event){
	// Find the appropriate requestAnimationFrame 
	window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
	
	// The main canvas
    var canvas = document.querySelector('#mainscreen');
    canvas.focus();
    
    var canvasContainer = document.querySelector('#canvasContainer');
    
    // A description of the current tile
	var descriptionBox = document.querySelector('#sidebox .description');
	var cursorInfo = document.querySelector('#cursorInfo');
	var unitInfo = document.querySelector('#unitInfo');
	
	var menu = document.querySelector('#menu');
	var menuUl = document.querySelector('#menu ul');
	
	// The type of tile the token is currently standing on
	var typeBox = document.querySelector('#sidebox .type');
	
    var ctx = canvas.getContext('2d');

    // Currently houses all images
    var imageDir = "/game/battlefield/resources/";
    var atlas = {};
    
    /**
     * For an instance called bindings, bindings.<code>command</code>.key will get the string name of the key. 
     * The keyCode property is the KeyEvent.keyCode property for the key.
     * 
     *  @constructor
     *  @memberof Engine
     *  @this KeyBindings
     *  @classdesc Manage key bindings for the game. An instance of this class can be used to get the key and keyCode of
     *  the computer keys assigned to commands.
     */
    function KeyBindings(){    	
    	var defaultKeyBindings = {
			select: {
				keyCode: 90,
				key: 'z'
			},
			cancel: {
				keyCode: 88,
				key: 'x'
			},
			info: {
				keyCode: 73,
				key: 'i'
			},
			left : {
				keyCode: 37,
				key: 'Left'
			},
			up : {
				keyCode: 38,
				key: 'Up'
			},
			right : {
				keyCode: 39,
				key: 'Right'
			},
			down : {
				keyCode: 40,
				key: 'Down'
			}
		};
    	
    	/*
    	 * This KeyBindings object will have the same properties as defaultKeyBindings, except with a command
    	 * property added to each one, as well as these default key binding objects duplicated at keys corresponding
    	 * to the keyCode. 
    	 */
    	for(var command in defaultKeyBindings){
    		var keycodeIndex = defaultKeyBindings[command].keyCode;    		
    		this[command] = defaultKeyBindings[command];
    		this[command].command = defaultKeyBindings[command].key;
    		this[keycodeIndex] = this[command];
    		
    	}
    }
    
    /**
     * Translate event.key to a key name listed in this class. The text returned by event.key for a key pressed on the keyboard is mostly uniform 
     * across browsers. There are a few differences, one of which is that Firefox appends the word "Arrow" onto the arrow directional keys.
     */
    KeyBindings.prototype.filterEventKey = function (key){
    	filter = /^Arrow/;
    	if(key.match(filter)){
    		return key.replace(filter, '').toLowerCase();
    	}
    }
    var bindings = new KeyBindings();
    
    // The mute button is checked by default
    var mute = document.querySelector('#mute');
    if(window.localStorage.getItem('mute') === null){
    	window.localStorage.setItem('mute', 'off');
    }
    mute.checked = window.localStorage.getItem('mute') === 'on' ? true : false;
    
    // Load the atlas JSON into the atlas object.
    function xhrListener () {
      atlas = JSON.parse(this.responseText);
      makeBattleMap();
    };

    // Get the atlas JSON via XHR.
    var xhr = new XMLHttpRequest();
    xhr.addEventListener("load", xhrListener);
    xhr.open("get", imageDir + "atlas.json", true);
    xhr.send();
    
    /**
    Draws the birdseye-view map for this encounter.
    @namespace Battlemap
    */
    function makeBattleMap(){
        /*
        The width and height of the map, corresponding with
        the number of tiles.
        */
        var gridWidth = 15;
        var gridHeight = 15;
        
        /*
        The dimensions of each tile.
        */
        var tileWidth = 40;
        var tileHeight = 40;
        
        /*
        The number of pixels from the left and top
        edge where the grid will be drawn.
        */
        var gridXOffset = 50;
        var gridYOffset = 50;        
        
        /**
         * 
         * Make a new SoundContrller
         * 
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.SoundController
         * @classdesc Much borrowed from {@link https://www.udacity.com/course/cs255 Google's Udacity HTML 5 Game Development course} 
         */
        function SoundController(){
        	/**
        	 * The sound clips to be controlled
        	 * @member {object}
        	 */
        	this.clips = {};
        	/**
        	 * Whether or not this controller is enabled
        	 * @member {boolean} 
        	 */
        	this.enabled = true;
        	/**
        	 * Represents all aspects of the sound played
        	 * @member {AudioContext}
        	 */
        	this.context = null;
        	/**
        	 * Used to control volume
        	 * @member {GainNode}
        	 */
        	this.mainNode = null;
        }

        /**
         * Prepare the audio context
         * @this Battlemap.SoundController
         * @function Battlemap.SoundController#init
         */
        SoundController.prototype.init = function(){
        	window.AudioContext = window.AudioContext || window.webkitAudioContext;
        	this.context = new AudioContext();;
        	this.mainNode = this.context.createGain();
        	this.mainNode.connect(this.context.destination);        	
            
            if(mute.checked){
            	sc.mute(mute.checked);
            }
            
            mute.addEventListener('click', function(event){
            	var status = !!this.checked;
            	var setting = status ? 'on' : 'off';
            	window.localStorage.setItem('mute', setting);
            	sc.mute(!!this.checked);
            });
        };
        
        /**
         * Load the sound to be played
         * 
         * @this Battlemap.SoundController
         * @function Battlemap.SoundController#load
         * @param {string} path The location of the sound
         * @param {function} callback The action to be performed on the sound
         * @returns {Battlemap.Sound}
         */
        SoundController.prototype.load = function(path, callback){
        	var thisSC = this;
        	this.init();
        	
        	if(sc.clips[path]){
        		callback(sc.clips[path].sound);
        		return gsm.clips[path].sound;
        	}
        	
        	var clip = {
        		sound: new Sound(),
        		buffer: null,
        		loop: false
        	};
        	sc.clips[path] = clip;
        	clip.sound.path = path;
        	
        	var request = new XMLHttpRequest();
        	request.open('GET', path, true);
        	request.responseType = 'arraybuffer';
        	request.onload = function () {
        		thisSC.context.decodeAudioData(request.response, function(buffer){
        			var clip = thisSC.context.createBufferSource();
        			clip.buffer = buffer;
        			clip.loop = true;
        			clip.connect(thisSC.mainNode);
        			clip.start = clip.start || clip.noteOn;
        			clip.start(0);		
        		});
        	};

        	request.send();
        };

        /**
         * Actually play the sound
         * @this Battlemap.SoundController
         * @function Battlemap.SoundController#playSound
         * @param {string} path The path to the sound
         * @param {object} settings An object with properties set to the sound's settings
         * @returns {boolean}
         */
        SoundController.prototype.playSound = function(path, settings){
        	if(!sc.enabled) return false;
        	
        	var looping = false;
        	var volume = 1;
        	
        	if (settings) {
        		if (settings.looping) looping = settings.looping;
        		if (settings.volume) volume = settings.volume;
        	}
        	
        	var sd = this.clips[path];
        	if (sd === null) return false;
        	if (sd.loop === false) return false;

        	var currentClip = null;
        	currentClip.buffer = buffer;
        	currentClip.gain.volume = voluem;
        	currentClip.loop = looping;
        	
        	clip.connect(mainNode);
        	clip.start = clip.start || clip.noteOn;
        	clip.start(0);	
        };

        /**
         * Get ready to play the sound at the passed in path
         * 
         * @param {string} path The location of the sound
         * @this Battlemap.SoundController
         * @function Battlemap.SoundController#play
         */
        SoundController.prototype.play = function(path){
        	sc.load(path, function(sObj){
        		sObj.play();
        	});
        };
        
        /**
         * Mute or unmute the sound
         * 
         * @param {boolean} mute If true, then mute the sound
         * @this Battlemap.SoundController
         * @function Battlemap.SoundController#mute
         */
        SoundController.prototype.mute = function(mute){
        	if(this.mainNode.gain.value > 0 || mute) {
        		this.mainNode.gain.value = 0;
    		}
    		else {
    			this.mainNode.gain.value = 1;
    		}
        };
        
        /**
         * Stop all sounds
         * 
         * @this Battlemap.SoundController
         * @function Battlemap.SoundController#stopAll
         */
        SoundController.prototype.stopAll = function(){
        	this.mainNode.disconnect();
    		this.mainNode = this.context.createGainNode(0);
    		this.mainNode.connect(this.context.destination);
        };
        
        /**
         *  Create a single sound
         * 
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.Sound
         * @classdesc A single sound
         */
        function Sound(){
        	/** The file path to this sound 
        	 * @member {string}
        	 * */
        	this.path = '';
        }

        /**
         * Play this sound 
         * 
         * @param {boolean} loop Whether or not the sound will repeat indefinitely
         * @this Sound
         * @function Battlemap.Sound#play
         */
        Sound.prototype.play = function(loop){
        	var settings = {volume: 1, looping: loop};
        	sc.playSound(this.path, settings);
        };
        
        /**
         * Create a new AI controller for automatically managing non player-controlled teams.
         * 
         * @param {Battlemap.Units} team The team to be automatically controlled
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.AI
         * @classdesc Control the decision making and command execution for non player-controlled teams. This class uses TurnController
         * 			  to automate its actions.
         */
        function AI(team){
        	var thisAI = this;
        	/**
        	 * The team under AI control
        	 * @member {Battlemap.Units}
        	 */
        	this.team = team;

        	/**
        	 * This is true if the AI is currently controlling a turn
        	 * @member {boolean}
        	 */
        	this.enabled = true;
        	
        	/**
        	 *  Actions assigned to each token on the team controlled by AI.
        	 *  @member {array}
        	 */
        	this.actions = [];
        	/**
        	 * The indexes that the units either have or will move to. This is used to
        	 * make sure that each unit is moving to a different location.
        	 * @member {array}
        	 */
        	this.destinations = [];
        	
        	/**
        	 * Choose and execute the unit menu command for the TurnController's current token 
        	 * @param {Event} event The event object
        	 */
        	this.menuHandler = function(event){
         		if(event.animationName === 'active'){
         			// Find the list item whose textContent names the command we want
             		var menuItems = document.querySelectorAll('#menu li');
             		var command = '';
             		
             		var activeCommand = thisAI.actions[turnController.currentIndex].chooseCommand();
             		
             		Array.prototype.some.call(menuItems, function(element){
             			command = element.textContent;
             			
             			if(command === activeCommand){
             				element.className = 'active';
             				return true;
             			}
             		});
             		
             		/*
         			 * The unit menu will stay above the unit for a second while the command is chosen
         			 */ 
     				switch(command){
         				case 'Move':
         					function selectMoveCommand(){
         						// Select the Move command
                 				activeMenu.menuHandler({keyCode: bindings.select.keyCode});
                 				// Start the movement

                 				thisAI.actions[turnController.currentIndex].move();
         					}
         					window.setTimeout(selectMoveCommand, 1000);
         				break;
         				
         				case 'End':
         					function selectEndCommand(){
         						// Select the End command
                 				activeMenu.menuHandler({keyCode: bindings.select.keyCode});
                 				var nextUnit = turnController.getNextUnit();
                 				if(!(nextUnit instanceof Token)){
                 					thisAI.enabled = false;
         						} else {
         							menu.addEventListener('animationend', theAI.start);
         							menu.addEventListener('webkitAnimationEnd', theAI.start);
         						}
         					}
         					window.setTimeout(selectEndCommand, 1000);
         				break;
     				}
         		}
         	};
        	
        	/*
        	 * Go through each token on the team and assign them actions.
        	 */
        	for(var i in this.team.tokens){        		
        		this.actions[i] = {
        				/**
        				 * The token for which the action is assigned 
        				 * 
        				 * @function Battlemap.AI#actions~token
        				 */
            			token: thisAI.team.tokens[i],
            			/**
            			 * Investigate the situation of each unit to determine
            			 * what their first command should be. For now, it will be
            			 * 'Move' by default. If they cannot move, they will end. 
            			 */
            			chooseCommand: function(){
            				var currentItem = document.querySelector('#menu.active li.active');
            				currentItem.classList.remove('active');
            				/*
            				 * The unit will only move if the internal checks have been
            				 * met and a destination can be found.
            				 */
            				if(this.canMove() && this.findDest() && this.findNextIndex()){
            					this.activeCommand = 'Move';
            				} else{
            					this.activeCommand = 'End';
            				}
            				
            				return this.activeCommand;
            			},
            			activeCommand: '',
            			/**
            			 * The index that the unit would like to reach
            			 */
            			destIndex: null,
            			/**
        				 * The next index the unit will move to
        				 */
            			nextIndex: null,
            			destRow: null,
            			destColumn: null,
            			/**
            			 * Only move if the unit has not reached the destination, the destination is
            			 * unoccupied, and the token has not yet moved.
            			 * 
        				 * @function Battlemap.AI#actions~token
            			 */
            			canMove: function(){
            				var reached = false;
            				
            				for(var tile in background.data[this.token.dataIndex]){
            					if(background.data[this.token.dataIndex][tile].description === 'A resource yet to be acquired'){
            						reached = true;
            						break;
            					}
            				}
            				
            				var notThereYet = this.token.dataIndex !== this.destIndex && !reached;
            				var unoccupied = thisAI.team.data[this.destIndex] === undefined || 
            								 thisAI.team.data[this.destIndex].length === 0;
            				return notThereYet && unoccupied && !this.token.moved;
            			},
            			move: function(){
            				cursor.setPosition(this.nextIndex);

            				// Select the command after a second
            				window.setTimeout(function(){
            					cursor.unitKeyMove({keyCode: bindings.select.keyCode});
            				}, 1000);
            			},
            			/**
            			 * Find the next place the unit can actually move to
            			 */
            			findNextIndex: function(){
            				var thisAction = this;
            				/*
            				 * The maximum number of rows that the unit can move within their range
            				 */
            				var maxRows, 
	            				/*
	            				 * The maximum number of columns that the unit can move within their range
	            				 */
            					maxColumns, 
            					/*
            					 * The number of rows from the unit's current position where locations
            					 * for movement are being tested
            					 */
            					rowDiff, 
            					/*
            					 * The number of rows from the unit's current position where locations
            					 * for movement are being tested
            					 */
            					columnDiff,
            					/*
            					 * A temporary holder for the location to move the unit 
            					 */
            					tempNextIndex;
            				/*
            				 * If the destination is in range, go ahead and move to there 
            				 */
            				var destInRange = this.token.indexesInRange.some(function(element){
            					if(element === thisAction.destIndex) return true;
            				});
            				
            				/*
            				 * For testing, the unit and its final intended destination
            				 */
            				window.console.log('Destination: ' + this.destIndex + ' for ' + Units.activeToken.type);
            				
            				/*
            				 * If the destination is in range, go ahead and move there
            				 */
            				if(destInRange){
            					this.nextIndex = this.destIndex;            					
            				} else{
            					/*
            					 * The destination was not in range, so we'll first try to reach its
            					 * row and then its column.
            					 */
            					var coordinates = indexToCoordinates(this.token.dataIndex);
            					var currentRow = coordinates.gridY;
            					var currentColumn = coordinates.gridX;            					 
            					
            					/*
            					 * Get the maximium amount of rows or columns the token can move in their range
            					 * in order to get as close as possible to their destination.
            					 */
            					function getMaxDirection(rowOrColumn){
            						var limit, direction, sign, maxDirection;
            						
            						/*
            						 * The number of rows or columns the token must move to reach the destination row or column.
            						 * For columns, a positive number moves to the right and a negative number moves to the left.
            						 * For rows, a positive number moves down the map and a negative number moves up.
            						 */
            						if(rowOrColumn === 'row'){
            							direction = thisAction.destRow - currentRow;
            						} else {
            							direction = thisAction.destColumn - currentColumn;
            						}
            						sign = direction < 0 ? -1 : 1;
            						
            						/*
            						 * The number of rows/columns that can be moved must be limited by the 
            						 * token's range. 
            						 */
            						maxDirection = Math.min(Math.abs(direction), thisAction.token.range);
            						maxDirection = maxDirection * sign;
            						
            						if(rowOrColumn === 'row'){
            							limit = maxDirection * gridWidth;
            						} else {
            							limit = maxDirection;
            						}
            						
            						return limit;
            					}
            					
            					maxRows = getMaxDirection('row');
            					maxColumns = getMaxDirection('column');
            					
            					rowDiff = maxRows;
            					columnDiff = maxColumns;
            					
            					tempNextIndex = thisAction.token.dataIndex + maxRows + maxColumns;
            					
            					/*
            					 * Test ranges in the direction of the final destination to see if the unit can move there
            					 */
            					while(units.occupied(tempNextIndex) && tempNextIndex !== thisAction.token.dataIndex){
            						if(columnDiff === 0){
            							rowDiff--;
            							columnDiff = maxColumns;
            						} else {
            							columnDiff--;            							
            						}
            						tempNextIndex = thisAction.token.dataIndex + rowDiff + columnDiff;
            					}
            					
            					/*
            					 * No ideal location were available, so find another place to move unit so it can try
            					 * to find another ideal path.
            					 */
            					if(tempNextIndex === thisAction.token.dataIndex){
            						var prevIndex, difference = 1, directionsSwitched = false, keepTesting = true;
                					rowDiff = 0;
                					columnDiff = 0;
                					prevIndex = tempNextIndex;

                					tempNextIndex = thisAction.token.dataIndex + rowDiff + (columnDiff + difference);
                					
                					/*
                					 * We will keep testing for an alternate location as long as the unit can
                					 * move to a location in the row
                					 */
            						while(keepTesting){
            							/*
            							 * If moving the unit to the new column is out of range or the unit cannot move to the
            							 * temporary next location, either switch column testing direction or move to the next row
            							 */
            							if(Math.abs(columnDiff + difference) > thisAction.token.range || !canMovePiece(prevIndex, null, tempNextIndex)){
            								if(!directionsSwitched){
            									directionsSwitched = true;
            									difference = -difference;
            								} else {
            									rowDiff++;
            									columnDiff = 0;
            									prevIndex = tempNextIndex;
            									tempNextIndex = thisAction.token.dataIndex + rowDiff + columnDiff;
            									difference = -difference;
            									directionsSwitched = false;
            								}
            							}
            							
            							/*
            							 * After switching to a new row to test, see if the temp location is on the map. 
            							 * If it isn't, we will assume all alternatives have been exhausted.
            							 */
            							if(canMovePiece(prevIndex, null, tempNextIndex)){
            								/*
            								 * If location is not occupied, we will move there. Otherwise,
            								 * test the next column
            								 */
            								if(!units.occupied(tempNextIndex)){
            									break;
            								} else {
            									columnDiff = columnDiff + difference;
            									tempNextIndex = thisAction.token.dataIndex + rowDiff + columnDiff;
            								}
            							} else {
            								tempNextIndex = thisAction.token.dataIndex;
            								keepTesting = false;
            								break;
            							}
            						}
        						}
            				}
            				
            				/*
            				 * At this point, we have either found the next location to move the unit,
            				 * or the unit will stay where they are.
            				 */
            				if(tempNextIndex !== thisAction.token.dataIndex){
        						this.nextIndex = tempNextIndex;
        						return this.nextIndex;
        					} else {
        						return false;
        					}
            			},
            			closestFound: false,
            			findDest: function(){
            				if(this.closestFound){
            					return this.closestFound;
            				}
            				
            				var foundTiles = [];
            				// Artificially increase the unit's move range to find the closest resource tile
                    		gridLoop: for(var r = 1; r < gridWidth; r++){
                    			Units.activeToken.showMoveRange(true, r);
                        		var ranges = Units.activeToken.indexesInRange;        		
                        		// Loop through every index in range
                        		rangeLoop: for(var index in ranges){
                        			/*
                        			 * Loop through every background tile in this background grid location.
                        			 * j is the index of each tile in the grid square. For example, if index 15 of the
                        			 * battlefield was a location the unit could move to, there could be
                        			 * a plain at background.data[15][0] and a bridge at background.data[15][1].
                        			 * Right now, we are just looking for neutral resources.
                        			 */
                        			tileLoop: for(var j in background.data[ranges[index]]){
                        				// AI units are only looking for neutral resources for now
                        				if(background.data[ranges[index]][j].description === 'A resource yet to be acquired'){
                        					var tempdest = Number(ranges[index]);                        					
                        					
                        					var destExists = thisAI.destinations.some(function(element){
                        						if(tempdest === element) return true;
                        					});
                        					
                        					/*
                        					 * Don't add indexes to the destinations array that
                        					 * are already there.
                        					 */
                        					if(destExists){
                        						continue rangeLoop;
                        					} else {
                        						/* A neutral resource was found.
                        						 * 
                        						 * Calculate its distance from the token.
                        						 */
                        						var tokenRow = Math.floor(Units.activeToken.dataIndex / gridWidth);
                        						var tokenColumn = Units.activeToken.dataIndex % gridWidth;
                        						
                        						var tileRow = Math.floor(tempdest / gridWidth);
                        						var tileColumn = tempdest % gridWidth;
                        						
                        						var rowDistance = Math.abs(tokenRow - tileRow);
                        						var columnDistance = Math.abs(tokenColumn - tileColumn);
                        						
                        						var distance = rowDistance + columnDistance;
                        						
                        						foundTiles.push({index: tempdest, distance: distance});
                        					}
                        				}
                        			}
                        		}
                        		
                        		/*
                        		 * If any tiles were found, determine which one will be moved to
                        		 */
                        		if(foundTiles.length > 0){
                        			/*
                        			 * First, we'll sort them in ascending order according to their
                        			 * row and column distance
                        			 */
                        			foundTiles.sort(function(a,b){
                        				return a.distance - b.distance;
                        			});
                        			
                        			// Now, we'll chose the one at the front of the array
                        			var chosenTile = foundTiles.shift();
                        			
                        			this.closestFound = true;
                        			
                        			this.destIndex = chosenTile.index;
            						thisAI.destinations.push(this.destIndex);
            						
            						var coordinates = indexToCoordinates(this.destIndex);
                					this.destRow = coordinates.gridY;
                					this.destColumn = coordinates.gridX;
                        			
                        			break;
                        		}
                    		}
                    		// Clear any changes to indexesInRange
                			Units.activeToken.indexesInRange = [];
                			
                			return this.closestFound;
            			}
            	};
        		
        		
        	}
        }
        
        /**
         * Start the unit's first action
         * 
         * @this Battlemap.AI
         * @function Battlemap.AI#start
         */
        AI.prototype.start = function(){        	
    		// Remove this listener until the next unit is ready to start
        	menu.removeEventListener('animationend', theAI.start);
        	menu.removeEventListener('webkitAnimationEnd', theAI.start);
    		
        	this.enabled = true;
        	menu = document.querySelector('#menu');
        	menu.addEventListener('animationend', this.menuHandler);
        	menu.addEventListener('webkitAnimationEnd', this.menuHandler);
        	
        	// Show the unit menu. The cursor should be on the active token
        	cursor.execHandler({keyCode: bindings.select.keyCode});
        };
        
        /**
         * Create the object for managing turns
         * 
         * @param {array} teams An object of Units objects for each of the teams on the battlefield
         * @this Battlemap.TurnController
         * @memberof Battlemap
         * @constructor
         * @classdesc Manage the turns for each unit and team (faction). 
         */
        function TurnController(teams){
        	/**
        	 * The current index of the active Token in the 
        	 * this.teams array
        	 */
        	this.currentIndex = 0;
        	/**
        	 * The teams, Units, on the battlefield
        	 */
        	this.teams = teams;
        	this.teams.init();
        }
        
        /**
         * Start a team's turn. 
         * 
         * @param {Battlemap.Units} team The units for the team (faction) whose turn is starting
         * @this Battlemap.TurnController
         * @function Battlemap.TurnController#startTurn
         */
        TurnController.prototype.startTurn = function(team){
        	/*
			 * If we're at the end of the teams list, assume that every team has had a turn
			 * and advance a day.
			 */
    		if(units.currentUnits === 0){        			
    			day++;
    			var h1 = document.querySelector('#days h1');
    			h1.textContent = 'Day ' + day;
    			days.appendChild(h1);
    			days.classList.remove('inactive');
    			    			
    			window.setTimeout(function(){
    				days.classList.add('inactive');
    			}, 5000);
    		}
    		
        	// When a team's turn starts, reset each unit's availability
        	team.tokens.forEach(function(element){
        		element.ended = false;
        		element.moved = false;
                element.actioned = true; // For now, start out with units already having attacked
        	});        	
        	
        	activeMenu = new UnitMenu();
        	/*
        	 * Only redraw the active team
        	 */
        	team.draw();
        	
        	// The first active token is the first one in the team's list of tokens
        	Units.activeToken = team.tokens[this.currentIndex];
        	
        	// Create a new cursor and draw it at the active token's location
        	cursor = new Cursor();
            cursor.dataIndex = Units.activeToken.dataIndex;
            cursor.draw(cursor.dataIndex);
            
            // Teams labeled "enemy" will be controlled by AI
            if(team.player === 'enemy'){
            	if(!theAI.team){
            		theAI = new AI(team);
            	}
            	theAI.start();
            	cursor.removeHandlers();
        		activeMenu.removeHandlers();
            } else {
            	if(theAI){            		
            		menu.removeEventListener('animationend', theAI.menuHandler);
            		menu.removeEventListener('webkitAnimationEnd', theAI.menuHandler);
            		menu.removeEventListener('animationend', theAI.start);
            		menu.removeEventListener('webkitAnimationEnd', theAI.start);
            	}
            }
        };
        
        /**
         * Give control to the next unit or call the next team
         * 
         * @this Battlemap.TurnController
         * @function Battlemap.TurnController#nextUnit
         */
        TurnController.prototype.nextUnit = function(){
    		var tc = this;
        	var nextUnit = this.getNextUnit();

        	if(nextUnit instanceof Token){
    			// Make the next unit the active token
        		Units.activeToken = nextUnit;
        		/* Assign the TurnController's currentIndex to that of the
        		 * activeToken as represented in currentUnits.
        		 */
    			currentUnits.tokens.every(function(element, index, array){
            		if(element === Units.activeToken){
            			tc.currentIndex = index;
            			return false;
            		} else {
            			return true;
            		}
            	});
    			
    			// Put the cursor on the current token
    			cursor = new Cursor();
    			cursor.dataIndex = Units.activeToken.dataIndex;
                cursor.draw(cursor.dataIndex);
                
    		} else {
    			// If there were no available units, give control to the next team.
    			this.nextTeam();
    		}
        	
        };
        
        /**
         * Give control to the next team
         * 
         * @this Battlemap.TurnController
         * @function Battlemap.TurnController#nextTeam
         * 
         */
        TurnController.prototype.nextTeam = function(){
        	var tc = this;
        	// Switch turns to the next team
        	function action(){
        		var nextTeam = units.currentUnits + 1;
        		if(nextTeam >= units.teams.length){        			
        			nextTeam = 0;
        		}
        		units.currentUnits = nextTeam;
        		tc.currentIndex = 0;
        		
        		currentUnits = units.teams[units.currentUnits];
    			tc.startTurn(currentUnits);        			
    		}
        	
        	// Wait a second before switching teams
        	window.setTimeout(action, 1000);
        };
        
        /**
         * Return the next Unit or return false if all units
         * on the team have ended their turn. This method should only
         * query the next unit and not make any assignments that affect
         * the state of the TurnController or Units.
         * 
         * @this Battlemap.TurnController
         * @function Battlemap.TurnController#getNextUnit
         * @returns {boolean|Battlemap.Unit}
         */
        TurnController.prototype.getNextUnit = function(){
        	var nextIndex, nextTeam, tc = this;
        	
        	// Loop back to the beginning if need be
        	if(this.currentIndex + 1 === currentUnits.tokens.length){
        		nextIndex = 0;
        	} else {
        		nextIndex = this.currentIndex + 1;
        	}
        	
        	/*
        	 * Find the next available unit on the team. If there are no available units,
        	 * break out of this loop.
        	 */
        	while(!currentUnits.tokens[nextIndex].isAvailable()){        		
        		if(currentUnits.tokens[nextIndex] === Units.activeToken){
        			/*
            		 * We've gone through every unit on the team and none were available, so the
            		 * team's turn is over. 
            		 */
        			nextTeam = true;
        			break;
        		}
        		if(nextIndex + 1 === currentUnits.tokens.length){ // Loop back to the beginning if need be
            		nextIndex = 0;
            	} else {
            		nextIndex++;
            	}
        	}
        	
        	/*
        	 * If there were no available units, every unit on this team has expended their turn, 
        	 * so this function should return false. Otherwise, return a reference to the 
        	 * next available unit.
        	 */
        	if(nextTeam){
        		return false;
        	} else {
        		return currentUnits.tokens[nextIndex];
        	}
        };
        
        TurnController.prototype.previousUnit = function(){
        	
        };
        
        TurnController.prototype.getPreviousUnit = function(){
        	
        };
        
        /*
        These constructors and objects represent the battlefield's background and units. 
        */
        
        /**
         * Create a map of the object data in atlas.json with keys for the sprite types
         * 
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.Frames
         * @classdesc <p>A container for the atlas frames with dynamically created properties named
         * after the frame types. The purpose of this class is to provide
         * a single place to store the information from atlas.json and create objects for
         * the engine that contain the coordinates of the sprites in the atlas.</p>
         * 
         * <p>The unit sprites are in properties named after the unit type which
         * refer to objects with properties for the different unit states (idle, attention, etc.). These 
         * properties contain arrays of objects of the sprite's coordinates in the atlas. The background sprites
         * are in arrays named after their type. For example, the Frames object 
         * <code>framesByType</code> has an array of the atlas coordinates for the walking movement 
         * of the Red Blades Fighter in the array <code>framesByType.fighter.walk</code>. The atlas coordinates 
         * of plains tiles is in the <code>framesByType.plains</code> property.</p>
         */
        function Frames(){
        	/**
        	 * Determined by the 'type' property of each frame in the atlas
        	 */
        	var frameType;
        	/**
        	 * Determined by the 'category' property of each frame in the atlas
        	 */
        	var frameCategory;
        	for(var sprite in atlas.frames){
        		frameType = atlas.frames[sprite].type;
        		frameCategory = atlas.frames[sprite].category; 
        		
        		if(frameCategory === 'units'){
        			// Create the object if it doesn't exist yet
    				if(!this[frameType]){
    					/*
    					* Properties for the frames of each unit state
    					*/
    					this[frameType] = {
    						idle : [],
    						attention: [],
    						end: [],
    						walk : {
    							Up : [],
    							Right : [],
    							Down : [],
    							Left : [],
    						}
    					};
    				}
    				
    				/**
    				This will name the properties of more specific states, such as
    				the direction of movement when walking
    				*/
    				var action;
    				
    				/*
    				* The frame data for each sprite is in a property whose name begins
    				* with the capitalized frame type.
    				*/
    				var firstLetter = frameType.substr(0,1);
    				var capital = firstLetter.toUpperCase();
    				var capitalized = capital + frameType.slice(1);
    				
    				var walkFrames = capitalized + " Walk (\\w+) (\\d)";
    				var idleFrames = capitalized + " Idle (\\d)";
    				var attEndFrames = capitalized + " (Attention|End)\\s*(\\d*)";
    				
    				/*
    				* Load each Frames sprite property with the frame data for
    				* that sprite state.
    				*/
    				var walk = sprite.match(new RegExp(walkFrames, 'i'));
    				if(walk !== null && walk.length === 3){
    					action = walk[1];
    					if(action === 'Up' || 
    					   action === 'Right' ||
    					   action === 'Down' ||
    					   action === 'Left'){
    						this[frameType].walk[action][walk[2] - 1] = atlas.frames[sprite];
    					}
    				} else {
    					var idle = sprite.match(new RegExp(idleFrames, 'i'));
    					if(idle !== null && idle.length === 2){
    						this[frameType].idle[idle[1] - 1] = atlas.frames[sprite];
    					} else {
    						var attentionOrEnd = sprite.match(new RegExp(attEndFrames, 'i'));
    						if(attentionOrEnd !== null && attentionOrEnd.length === 3){
    							var propertyName = attentionOrEnd[1].toLowerCase();
    							this[frameType][propertyName].push(atlas.frames[sprite]);
    						}
    					}
    				}
        		} else {
        			var frameProp = frameType ? frameType : frameCategory;
    				// Create the array if it doesn't exist yet				
    				if(!Array.isArray(this[frameProp])){
    					this[frameProp] = [];
    				}

    				/*
    				 * This Frames object will have the atlas data saved
    				 * for background tiles.
    				 */				
    				this[frameProp].push(atlas.frames[sprite]);
        		}
        	}
        }
        var framesByType = new Frames();
        
        /**
         * Create a menu. This is the base class for the different types of menus used in the game. Any class structure used
         * to represent a list of options that a user can scroll through to select, queue up, and cancel/rollback commands should
         * be based on this class.
         * 
         * @constructor
         * @this Battlemap.Menu
         * @memberof Battlemap
         * @classdesc The base class for all menus that provides keyboard arrow navigation for vertically scrolling a list
         *            of commands that can be queued and rolled back.
         */
        function Menu(){
        	menu.addEventListener('animationend', this.animationHandler);
        	menu.addEventListener('webkitAnimationEnd', this.animationHandler);
        }
        
        /**
         * Basic animation handling for a menu
         * @param event
         */
        Menu.prototype.animationHandler = function(event){
        	if(event.animationName === 'active'){
    			menuUl.removeAttribute('class');
    		} else if(event.animationName === 'inactive'){
    			this.className = 'closed';
    		}
        };
        
        /** 
         * The current queue of commands that can be rolled back 
         * @member {array}
         * */
        Menu.prototype.commandQueue = [];
        
        /** 
         * The handlers registered for the menu. This array will be cleared
         * in {@link Battlemap.Menu#removeHandlers|removeHandlers()}. 
         * @member {array}
         * */
        Menu.prototype.registeredHandlers = [];
        
        /** 
         * The command that is currently being executed
         * @member {string} 
         * */
        Menu.prototype.activeCommand = '';
        
        /** 
         * The previously executed command
         * @member {string} 
         * */
        Menu.prototype.previousCommand = '';
        
        /**
    	 * Handle keydown events for the menu.
    	 * @this HTMLCanvasElement
    	 * @param {Event} event The event object for the key pressed
    	 */
        Menu.prototype.menuHandler = function(event){
        	// Prevent scrolling the page
        	if(event.preventDefault){
        		event.preventDefault();
        	}
        	
        	switch(event.keyCode){
	    		case bindings.select.keyCode: // Select command
	    			activeMenu.selectCommand();
	    		break;
	    		case bindings.cancel.keyCode: // Cancel (Rollback)
	    			activeMenu.cancelCommand();
	    		break;
	    		// Up
	    		case bindings.up.keyCode: // Move cursor up
	    			activeMenu.moveCursor('Up');
	    		break;
	    		// Down
	    		case bindings.down.keyCode: // Move cursor down
	    			activeMenu.moveCursor('Down');
	    		break;
	    	}
        };
        
        /**
         * Clear the last command.
         * @this Battlemap.Command|Battlemap.Menu
         */
        Menu.prototype.clearCommand = function(){
        	
        };
        
        /**
         * Add the showMenu command to the commandQueue of the entity whose commands this menu is managing.
         * @this Battlemap.Command|Battlemap.Menu
         */
        Menu.prototype.addShowMenuCommand = function(){
        	
        }
        
        /**
         * Start the next command in this menu's managed command queue
         * @this Battlemap.Command|Battlemap.Menu
         */
        Menu.prototype.startCommand = function(){
        	
        }
        
        /**
         * Whether or not this menu can be shown right now
         * @this Battlemap.Command|Battlemap.Menu 
         */
        Menu.prototype.canShowMenu = function(){
        	
        };
        
        /**
         * Show the menu. This method is expected to be assigned to the {@link Battlemap.Command#start|start()} method of
         * a {@link Battlemap.Command|Command}, where the <code>this</code> keyword refers to the Command object. 
         * It might also be called as a method of an object shared across the battlefield. 
         * @this Battlemap.Command|Battlemap.Menu
         * @param {string} unitType The type of unit which determines which commands are shown
         */
        Menu.prototype.showMenu = function(unitType){
        	
        };
        
        /**
         * Hide the menu
         * 
         * @this Battlemap.Command|Battlemap.Menu
         */
        Menu.prototype.hideMenu = function(){
        	
        };
        
        /**
		 * Change the selected command by removing the "active" class from
		 * the currently selected list item. The "active" class
		 * places the pointy hand background to the left of the
		 * list item
		 * 
		 * @param {string} direction The direction to move the cursor. Either "Up" or "Down"
		 */
        Menu.prototype.moveCursor = function(direction){
    		/*
    		Only respond to cursor movements if 
    		the menu is active
    		*/            		
    		if(menu.className !== 'active'){
    			return;
    		}
    		var currentItem = document.querySelector('#menu.active li.active');
    		var nextItem;
    		if(direction === 'Up'){
    			var nextItem = currentItem.previousElementSibling;
    			if(!nextItem) nextItem = document.querySelector('#menu.active ul').lastElementChild;
    			
    		} else {
    			var nextItem = currentItem.nextElementSibling;
    			if(!nextItem) nextItem = document.querySelector('#menu.active ul').firstElementChild;
    		}
    		
    		currentItem.classList.remove('active');
    		nextItem.classList.add('active');
    	};
    	
    	/**
    	 * Select the menu command
    	 */
    	Menu.prototype.selectCommand = function(){
    		
    	};
    	
    	/**
    	 * Rollback the menu command
    	 * @param {boolean} cancelLast Whether or not the last and only command should in the managed
    	 * 							   commandQueue should be canceled. 
    	 */
    	Menu.prototype.cancelCommand = function(cancelLast){
    		
    	};
    	
    	/**
         * Register the handlers for the unit menu and store them
         * in the registeredHandlers array property.
         * 
         */
        Menu.prototype.readyHandlers = function(){  
        	if(theAI.enabled){
        		return;
        	}
        	if(activeMenu.registeredHandlers.length === 0){
        		activeMenu.registeredHandlers = [
        		            {eventType: 'keydown', handler: activeMenu.menuHandler}
        		];
        	}
        	
        	for(var index in activeMenu.registeredHandlers){
        		canvas.addEventListener(activeMenu.registeredHandlers[index].eventType, activeMenu.registeredHandlers[index].handler);
        	}
        	
        };
    	
    	/**
         * Remove all event listeners from the canvas that are in
         * {@link Battlemap.Menu#registeredHandlers|registeredHandlers}.
         * 
         */
        Menu.prototype.removeHandlers = function(){
        	for(var i in this.registeredHandlers){
        		if(this.registeredHandlers[i].eventType && this.registeredHandlers[i].handler){
        			canvas.removeEventListener(this.registeredHandlers[i].eventType, this.registeredHandlers[i].handler);
        		}
        	}
        	
        	this.registeredHandlers = [];
        };
        
        /**
         * Create the unit's menu
         * 
         * @constructor
         * @extends Battlemap.Menu
         * @memberof Battlemap
         * @classdesc A common mechanism for tokens and other objects to issue commands 
         */
        function UnitMenu(){        	
        	Menu.call(this);
        }
        
        UnitMenu.prototype = new Menu;
        
        /**
         * Select the unit's active menu command
         */
        UnitMenu.prototype.selectCommand = function(){
        	var activeCommand = document.querySelector('#menu li.active').textContent;
			var commands = Units.activeToken.commands;
			
			for(var i in commands){
				if(commands[i].name === activeCommand){
					/*
					 * Don't respond to menu selection if the command has been exhausted this turn
					 * The command should also appear as disabled in the unit menu
					 */  
					if((activeCommand === 'Move' && Units.activeToken.moved) ||
					   (activeCommand === 'Attack' && Units.activeToken.actioned)){
						return;
					}
					
					// If the active command is disabled, don't do anything 
					for(var index in Units.activeToken.commands){
						if(commands[i].name === Units.activeToken.commands[index].name && !Units.activeToken.commands[index].enabled){
							return;
						}
					}
					
					activeMenu.activeCommand = commands[i].name;
					
					menuUl.className = 'closed';
					menu.className = 'inactive';
					
					Units.activeToken.commandQueue.push(new Command(commands[i].name, commands[i].action, activeMenu.clearCommand));
					
					activeMenu.removeHandlers();
					
					var currentCommand = Units.activeToken.commandQueue[Units.activeToken.commandQueue.length - 1];
					
					if(currentCommand.start){
						currentCommand.start(Units.activeToken.type);
					} else {
						activeMenu.removeHandlers();
						cursor.readyHandlers();
					}
					break;
				}
			}
        };
        
        UnitMenu.prototype.cancelCommand = function(cancelLast){
        	if(Units.activeToken.commandQueue.length > 0){
    			var lastCommand = Units.activeToken.commandQueue.pop(); 
    			lastCommand.cancel();
    			
    			var limit = Units.activeToken.commandQueue.length > 1;
    			if(cancelLast){
    				var limit = true;
    			}
    			
    			if(limit && Units.activeToken.commandQueue[Units.activeToken.commandQueue.length - 1].start){
    				Units.activeToken.commandQueue[Units.activeToken.commandQueue.length - 1].start(Units.activeToken.type);
    			} else if(Units.activeToken.commandQueue.length === 0){
    				this.removeHandlers();
    			}
    		}
        };
        
        /**
         * @this Battlemap.Command|Battlemap.UnitMenu
         */
        UnitMenu.prototype.addShowMenuCommand = function(){
        	// If the unit's command queue is empty, create a new command for showing and hiding the menu. 
			if(Units.activeToken.commandQueue.length === 0){
				Units.activeToken.commandQueue.push(new Command('activeMenu', this.showMenu, this.hideMenu));
			}
        }
        
        /**
         * @this Battlemap.Command|Battlemap.UnitMenu
         */
        UnitMenu.prototype.startCommand = function(){
        	Units.activeToken.commandQueue[Units.activeToken.commandQueue.length - 1].start(Units.activeToken.type);
        }
        
        /**
         * Clear the last command. At the moment, this only clears
         * the last move command.
         * @this Battlemap.Command|Battlemap.UnitMenu
         */
        UnitMenu.prototype.clearCommand = function(){
        	if(activeMenu.previousCommand === 'Move'){
        		Units.activeToken.moved = false;
        		// If clearing a move, make the location the token moved to undefined in units.data
        		currentUnits.data[Units.activeToken.dataIndex] = undefined;
        	}
        	// Clear the canvas, redraw the background, and draw the unit at their final position
        	background.redraw();
            cursor.setPosition(Units.activeToken.dataIndex);
            activeMenu.activeCommand = '';
        };
        
        UnitMenu.prototype.canShowMenu = function(){
        	// Don't select the token if its turn has ended
			return Units.activeToken.isAvailable();
        };
        
        UnitMenu.prototype.showMenu = function(unitType){
        	activeMenu.activeCommand = 'showMenu';
        	background.redraw();
            units.draw();
        	
        	// Close the cursorInfo box if open.
        	cursor.infoHandler({keyCode: bindings.info.keyCode}, true);
        	
        	/*
        	 * Define the commands that will be available in a unit's menu
        	 */
        	switch(unitType){
        		case 'dragon':
        		case 'marketh':
        			Units.activeToken.commands = [
	                     new MenuCommand('Attack', function(){console.log('Attack coming soon');}, false), 
	                     new MenuCommand('Move', Units.activeToken.moveCommand, Units.activeToken.moved ? false : true), 
	                     new MenuCommand('End',function(){Units.activeToken.endCommand();},true)
        			];
        		break;
        		case 'bandit':
        		case 'pit':        		
        			Units.activeToken.commands = [
	                     new MenuCommand('Steal', function(){console.log('Steal coming soon');}, false), 
	                     new MenuCommand('Move', Units.activeToken.moveCommand, Units.activeToken.moved ? false : true), 
	                     new MenuCommand('End',function(){Units.activeToken.endCommand();},true)
        			];
        		break;
        		default:
        			Units.activeToken.commands = [
                         new MenuCommand('Move', Units.activeToken.moveCommand, Units.activeToken.moved ? false : true), 
                         new MenuCommand('End',function(){Units.activeToken.endCommand();},true)
        			];
        		break;
        	}        	
        	
			menuUl.innerHTML = '';
			
			for(var index in Units.activeToken.commands){
				var disabled = '';
				if(!Units.activeToken.commands[index].enabled){
					disabled = ' class="disabled"';
				}
				
				menuUl.innerHTML += '<li' + disabled + '>' + Units.activeToken.commands[index].name + '</li>';
			}
			
			var firstItem = document.querySelector('#menu ul li:not(.disabled)');
			firstItem.className = 'active';
			
			var grid = indexToCoordinates(Units.activeToken.dataIndex);
			
			menu.style.left = grid.posX + 'px';
			var activeKeyframe, inactiveKeyframe, menuActive, menuInactive;
			var rules = document.styleSheets[0].cssRules;
			for(var i in rules){
				if (rules[i].name === 'active'){
				    activeKeyframe = rules[i];
				} else if(rules[i].name === 'inactive'){
					inactiveKeyframe = rules[i];
				} else if(rules[i].selectorText === '#menu.active'){
					menuActive = rules[i]; 
				} else if (rules[i].selectorText === '#menu.inactive'){
					menuInactive = rules[i]; 
				}
			}
			var newActiveTop = grid.posY - 110;
			menuInactive.style.top = newActiveTop + 'px';
			activeKeyframe.cssRules[1].style.top = newActiveTop + 'px';
			
			var newInactiveTop = newActiveTop + 30;
			menuActive.style.top = newInactiveTop + 'px';
			inactiveKeyframe.cssRules[1].style.top = newInactiveTop + 'px';
			menu.className = 'active';
			
			activeMenu.readyHandlers();
			cursor.removeHandlers();
        };
        
        /**
         * Hide the Unit menu
         * 
         * @this Battlemap.Command|Battlemap.UnitMenu
         */
        UnitMenu.prototype.hideMenu = function(){
        	if(activeMenu.previousCommand === 'Move'){
        		Units.activeToken.moved = false;
        		activeMenu.previousCommand = '';
        	}
        	menuUl.className = 'closed';
			menu.className = 'inactive';
			activeMenu.removeHandlers();
			
			cursor.readyHandlers();
			
			activeMenu.activeCommand = '';

			background.redraw();
            cursor.draw(Units.activeToken.dataIndex);
        };
        
        /**
         * Create a new command
         * 
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.Command
         * @param {string} name The command's name
         * @param {function} start The function that starts the command
         * @param {function} cancel Roll back this command
         * @classdesc A command the user executes for their unit. Commands have a start
         * and cancel method. 
         */
        function Command(name, start, cancel){
        	/** The command's name 
        	 * @member {string}
        	 * */
        	this.name = name;
        	/** The function that starts the command 
        	 * @member {function}
        	 * */
        	this.start = start;
        	/** Roll back this command 
        	 * @member {Function}
        	 * */
        	this.cancel = cancel;
        }
        
        /**
         * Create a new Unit Menu entry
         * 
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.MenuCommand
         * @param {string} name The command's name as it appears in the menu
         * @param {function} action The function associated with this command
         * @param {boolean} enabled Whether or not this command is currently enabled
         * @classdesc A command in the unit menu. 
         */
        function MenuCommand(name, action, enabled){
        	/** The command's name as it appears in the menu 
        	 * @member {string}
        	 * */
    		this.name = name;
    		/** The function associated with this command 
    		 * @member {function}
    		 * */
    		this.action = action;
    		/** Whether or not this command is currently enabled 
    		 * @member {boolean}
    		 * */
    		this.enabled = enabled;
    	}
        
        /**
         * Create the asset layer 
         * 
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.Layer
         * @param {number} rowMax The number of rows this layer should occupy
         * @param {number} columnMax The number of columns this layer should occupy
         * @classdesc A layer of assets on the map that are expected to be randomly drawn, unless
         * a source file was specified. The max row and column specify the maximum number 
         * of rows and columns that the tiles will appear in. Other Layer objects to be drawn in
         * such a manner will extend this class by using Layer.call() in its class's constructor 
         * as well as assigning "new Layer" to its class's prototype.
         */
        function Layer(rowMax, columnMax){
	    	/**
	    	 * A sprite object from the frames object in atlas.json. Each subclass
	    	 * defines its own resourceSpts array of possible assets for its type 
	    	 * in its prototype.
	    	 */
            this.spt = chooseResourceSpt();
            
            /**
             * The maximum number of rows in which sprites of this layer can appear
             */
            this.rowMax = Math.min(rowMax, gridHeight - 1);
            
            /**
             * The maximum number of columns in which sprites of this layer can appear
             */
            this.columnMax = Math.min(columnMax, gridWidth - 1);
            
            /** 
             * For now, choose a random asset from this Layer's resourceSpts that represents this layer.
             * 
             * @function Battlemap.Layer~chooseResourceSpt
             * @return {function} a function that will choose a random sprite for this layer whenever it is called. 
             */
            function chooseResourceSpt(){
                return function(){
                    return this.resourceSpts[Math.floor(Math.random() * this.resourceSpts.length)];
                };
            }
        }
        /**
         * Add this layer to the background
         * @this Battlemap.Layer
         * @function Battlemap.Layer#addToBackground
         * @param {Battlemap.Background} background The background on which to add this layer
         */
        Layer.prototype.addToBackground = function(background){
        	var row, column, dataIndex, dontPush;
            var rows = [], columns = [];
            for(var i = 0; i < this.rowMax; i++){
            	/*
            	 * Choose which rows will have sprites for this Layer
            	 */
                do{
                    row = Math.floor(Math.random() * gridHeight);
                } while(rows.indexOf(row) !== -1);
                rows.push(row);
                columns = [];
                
                for(var j = 0; j < this.columnMax; j++){
                	/*
                	 * Now the columns are chosen. Only add layer asset to unoccupied 
                	 * background location. The dataindex of the location in the 
                	 * background is computed here.
                	 */
                    do{
                        column = Math.floor(Math.random() * gridWidth);
                        dataIndex = row * gridHeight + column;
                        
                        /*
                         * Find out what kind of Layer this is to determine the condition
                         * for adding it to the dataIndex
                         */
                        var level1 = this instanceof RoughTerrain ||
                        			 this instanceof Water ||
                        			 this instanceof Plains;
                        
                        var level2 = this instanceof Castles ||
           			 				 this instanceof Resources ||
           			 				 this instanceof Villages ||
           			 				 this instanceof Trees;
                        
                        var occupied = background.occupied(dataIndex);
                        
                        if(level1){
                        	dontPush = occupied;
                        } else if(level2){
                        	if(!Array.isArray(occupied)){
                        		dontPush = true;
                        	} else {
                        		dontPush = occupied.length !== 1;
                        	}                        	 
                        } else {
                        	throw new Error('Could not identify type of ' + this.constructor);
                        }                        
                    } while((columns.indexOf(column) !== -1) || dontPush);
                    columns.push(column);
                    background.add(this.spt(), dataIndex);
                }
            }
        };
        
        /**
         * Make new castles
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.Castles
         * @classdesc The player and enemy castles
         */
        function Castles(){                        
            
        }
        
        /**
         * The asset and location of the enemy castle
         * @this Battlemap.Castles
         * @member {object} Battlemap.Castles#enemy
         */
        Castles.prototype.enemy = {
        		type: 'enemy',
                spt: atlas.frames["encampment_redblades.png"],
                dataIndex: null,
                row:  0, // Always at the bottom
                column: null
        };        
        /**
         * The asset and location of the player castle
         * @this Battlemap.Castles
         * @member {object} Battlemap.Castles#player
         */
        Castles.prototype.player = {
        		type: 'player',
                spt: atlas.frames["encampment_roric.png"],
                dataIndex: null,
                row: gridHeight - 1, // Always at the top
                column: null
        };
        /**
         * Choose a random column for this castle to appear in.
         * 
         * @this Battlemap.Castles
         * @function Battlemap.Castles#chooseColumn
         * @param {Battlemap.Background} background The background on which this castle will be drawn
         * @param {string} player Either 'enemy' or 'player'
         * @returns {number} The dataIndex in the background where this castle will be drawn
         */
        Castles.prototype.chooseColumn = function(background, player){
            var column, dataIndex, dontPush, occupied;
            /*
             * Find an unoccupied column in the given row in
             * which to place this castle.
             */
            do{
                column = Math.floor(Math.random() * gridWidth);
                dataIndex = player.row * gridHeight + column;
                
                occupied = background.occupied(dataIndex);
                dontPush = occupied.length !== 1;
            } while(dontPush);
            
            this[player.type].column = column;
            return dataIndex;
        };
        /**
         * Add this castle to the background
         * 
         * @this Battlemap.Castles
         * @function Battlemap.Castles#addToBackground
         * @param {Battlemap.Background} background The background this castle will be drawn on
         */
        Castles.prototype.addToBackground = function(background){                
            this.enemy.dataIndex = this.chooseColumn(background, this.enemy);
            background.add(this.enemy.spt, this.enemy.dataIndex);
            
            this.player.dataIndex = this.chooseColumn(background, this.player);
            background.add(this.player.spt, this.player.dataIndex);
        };
        
        /**
         * Make new resource tiles
         * 
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.Resources
         * @extends Battlemap.Layer
         * @param {number} rowMax The maximum number of rows on which this tile can appear
         * @param {number} columnMax The maximum number of columns on which this tile can appear
         * @classdesc Resource tiles  
         */
        function Resources(rowMax, columnMax){            
            Layer.call(this, rowMax, columnMax);
        }
        Resources.prototype = new Layer;
        Resources.prototype.resourceSpts = framesByType.resource;
        
        /**
         * Make new tree tiles
         * 
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.Trees
         * @extends Battlemap.Layer
         * @param {number} rowMax The maximum number of rows on which this tile can appear
         * @param {number} columnMax The maximum number of columns on which this tile can appear
         * @classdesc Tree tiles  
         */
        function Trees(rowMax, columnMax){            
            Layer.call(this, rowMax, columnMax);
        }
        Trees.prototype = new Layer;
        Trees.prototype.resourceSpts = framesByType.tree;
        
        
        /**
         * Make new village tiles
         * 
         * @constructor
         * @this Battlemap.Villages
         * @memberof Battlemap
         * @extends Battlemap.Layer
         * @param {number} rowMax The maximum number of rows on which this tile can appear
         * @param {number} columnMax The maximum number of columns on which this tile can appear
         * @classdesc Village tiles 
         */
        function Villages(rowMax, columnMax){            
            Layer.call(this, rowMax, columnMax);
        }
        Villages.prototype = new Layer;
        Villages.prototype.resourceSpts = framesByType.village; 
        
        /**
         * Make new rough terrain tiles
         * 
         * @constructor
         * @this Battlemap.RoughTerrain
         * @memberof Battlemap
         * @extends Battlemap.Layer
         * @param {number} rowMax The maximum number of rows on which this tile can appear
         * @param {number} columnMax The maximum number of columns on which this tile can appear
         * @classdesc Rough terrain tiles  
         */
        function RoughTerrain(rowMax, columnMax){
        	Layer.call(this, rowMax, columnMax);
        }
        RoughTerrain.prototype = new Layer;
        RoughTerrain.prototype.resourceSpts = [].concat(framesByType.forest)
        										.concat(framesByType.mountain)
        										.concat(framesByType.desert);
        
        /**
         * Make new bodies of water
         * 
         * @constructor
         * @this Battlemap.Water
         * @memberof Battlemap
         * @extends Battlemap.Layer
         * @param {number} rowMax The maximum number of rows on which this tile can appear
         * @param {number} columnMax The maximum number of columns on which this tile can appear
         * @classdesc Bodies of water 
         */
        function Water(rowMax, columnMax){
        	Layer.call(this, rowMax, columnMax);
        }
        Water.prototype = new Layer;
        Water.prototype.resourceSpts = framesByType.water;
        
        /**
         * Make new plains
         * 
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.Plains
         * @classdesc Empty grass fields, some of which may be partially submerged. 
         */
        function Plains(){
        	
        }
        /**
        Put all the plains tiles from the atlas in an array
        that will be used to draw random plains across the battlefield.
        
        @function Battlemap.Plains#resourceSpts
        */   
        Plains.prototype.resourceSpts = framesByType.plain;
        
        /*
        Load the atlas image.
        */
        var atlasPng = new Image();
        atlasPng.src = imageDir + "atlas.png";
        
        /*
         * Castles and Units should be available to all classes.
         */
        var castles, units, currentUnits, background, savedBackground, startDelay = false, 
        	cursor, activeMenu, turnController, theAI = {enabled: false}, sc, day = 0;
        
        /*
        When the atlas is loaded, draw the layers.
        */
        atlasPng.onload = function(){
        	savedBackground = window.localStorage.getItem('background');
        	background = new Background();
        	/*
             * See if we need to either draw a random map or
             * load a saved one. If we're loading a saved one, the turn
             * controller won't start until the map has finished loading.
             */
        	if(!savedBackground){        		          
                background.draw("random");
        	} else {
        		startDelay = true;
        		background.load();
        	}
            
        	// Load the stat info for Units
        	Token.levelStatMap.load();
        	
            // Setup the units
            units = {
            	teams: [
            	        new Units("player", "roric"), 
            	        new Units("enemy", "red blades")
            	],
            	currentUnits: 0,
            	draw: function(skipActive){
            		for(var i in this.teams){
            			this.teams[i].draw(skipActive);
            		}
            	},
            	init: function(){
            		for(var i in this.teams){
            			this.teams[i].init();
            		}
            	},
            	occupied: function(index){
            		var occupied = false;
            		/*
            		 * If the location has something there and it is an non-empty array, then the space is occupied 
            		 */
            		for(var team in this.teams){
            			if(this.teams[team].data[index] !== undefined && this.teams[team].data[index].length !== 0){
            				occupied = true;
            			}
            		}
            		return occupied;
            	}
            };
            
            currentUnits = units.teams[units.currentUnits];
            
            /*
             * Don't start the turn controller yet if a saved background is to be loaded.
             */
            if(!startDelay){
            	turnController = new TurnController(units);            
                turnController.startTurn(currentUnits, true);
            }            

            // Create the sound controller and start the music
            sc = new SoundController();
            sc.play('resources/Roric Theme.mp3');
            
            // Register the keys for user input
            registerKeys("player");
        };      
        
        /**
        Make a new background for the battlefield map
        
        @constructor
        @this Battlemap.Background
        @memberof Battlemap
        @classdesc <p>Sets up the background to be used for the battlefield map.</p>
        
        <p>The data array is a list of the {@link Layer} objects that will
        be drawn to the map. Each index corresponds to tile coordinates
        on the grid. E.g., (0,0) is the top left tile in the default 15 x 15 grid
        and its data would be placed in an array at data[0]. (2,1) would be 
        the third tile across and second one down with data[17] containing 
        its data. In a 15 x 15 grid, the first column of every row has a data index 
        that is a multiple of 15.</p> 
        
        <p>Each element in the data array is another array listing the sprites/tiles
        to be found at that location in the form of Layer objects.</p>
        
        <p>The grid coordinates are zero based so that multiplying the coordinate 
        with the pixel dimension of each tile gives you the pixel coordinate of 
        where the tile will be drawn, before considering any offset. For example,
        in a 15 x 15 grid, the objects in the array at data[210] would list
        the tiles to be found at the grid coordinates (0,14), the first tile
        in the last row. This tile would be drawn at:</p> 
        
        <blockquote>(gridX * tileLength, gridY * tileHeight)</blockquote>
        
        <p>or (0, 210) within the battlefield map. In the draw() function for 
        backgrounds and units, offsets are applied to translate these coordinates 
        to the map's position within the canvas element.</p>
        */
        function Background(){
        	/**
        	 * An array of Castle, Layer, and Plain sprites in the grid.
        	 * Each element of this array contains one or more
        	 * frames objects from atlas.json.
        	 * 
        	 * @member {array}
        	 */
            this.data = new Array(gridWidth * gridHeight);
            
            /** This background's castles 
             * @member {Battlemap.Castles}
             * */
            this.castles;
            /** This background's resources 
             * @member {Battlemap.Resources}
             * */
            this.resources;
            /** This background's villages 
             * @member {Battlemap.Villages}
             * */
            this.villages;            
            /** This background's rough terrain 
             * @member {Battlemap.RoughTerrain}
             * */
            this.roughTerrain;
            
        }
        
        /**
         * Add a frames object to a position in the background
         * @this Battlemap.Background
         * @param {object} spt The frames object to add to the specified position
         * @param {number} dataIndex The position where the layer will be added
         * @function Battlemap.Background#add
         */
        Background.prototype.add = function(spt, dataIndex){
            if(this.data[dataIndex] instanceof Array){
                this.data[dataIndex].push(spt);
            } else {
                this.data[dataIndex] = [spt];
            }
        };
        
        /**
         * Load the named background map. It will either be called on a Background or
         * as the handler for clicking the Load button in the demo.
         * @this HTMLButtonElement
         * @param {string|Event} mapfile <p>The file containing the map to load which should be in the map dir.</p>
         * 								  <p>This parameter could also be an event object if this method is called
         * 								  as a click event listener on the #Load button.</p>
         *  @function Battlemap.Background#load
         */
        Background.prototype.load = function(mapfile){
        	var evt = mapfile;
        	var filelist = document.querySelector('#filelist');
        	var messageBox = document.querySelector('#filelistcontainer .messages');
        	
        	/*
        	 * Either a map filename could be passed or we grab the one in
        	 * the file list. The map's file name may have also been saved
        	 * in localStorage.
        	 */
        	if(filelist){
        		/*
        		 * Load either the selected or saved background
        		 */
        		var selected;
        		if(evt instanceof Event){
        			selected = filelist.options[filelist.selectedIndex].value;
        		} else {
        			selected = savedBackground;
        		}
        		if(selected){
        			Array.prototype.some.call(filelist.options, function(element){
        				if(element.textContent === selected){
        					element.setAttribute('selected', 'selected');
        					mapfile = selected;
        					return true;
        				}
        			});
        		} else {
        			/*
        			 * If user clicked the button or no background is saved, load the
        			 * one selected.
        			 */
        			mapfile = filelist.options[filelist.selectedIndex].textContent;
        		}
        	}
        	
        	if(evt instanceof Event){        		
        		window.localStorage.setItem('background', mapfile);
        	}
        	
        	var xhr = new XMLHttpRequest();
        	if(!mapfile){
        		mapfile = savedBackground;
        	}
        	xhr.open('GET', '/game/mapeditor/maps/' + mapfile);
        	xhr.responseType = 'json';
        	
        	xhr.addEventListener('load', function(event){
        		
        		var downloadedMap = xhr.response;
        		
        		if(!(downloadedMap instanceof Array)){
        			messageBox.innerHTML = '<p>' + mapfile + ' is not an array</p>';
        			return;
        		}
        		
        		// The map needs to match this battlefield's grid size            		
        		var maplength = downloadedMap.length;
        		var expectedlength = gridWidth * gridHeight;
        		
        		if(downloadedMap.length !== expectedlength){
        			messageBox.innerHTML = '<p>' + mapfile + ' is of length ' + downloadedMap.length + ' but expected ' + expectedlength + '</p>';
        			return;
        		}
        		
        		// Every tile named in the downloaded map must be in the atlas
        		var valid = downloadedMap.every(function(square, squareIndex){
        			var squareCheck = square.every(function(element){
        				return !!atlas.frames[element];
        			});
        			return squareCheck;
        		});
        		
        		// The map should be good to load
        		if(valid){
        			downloadedMap.forEach(function(square, squareIndex){
        				var tileset = []
        				square.forEach(function(element){
        					tileset.push(atlas.frames[element]);
        				});
        				background.data[squareIndex] = tileset;
        			});
        			
        			background.redraw();
                    units.draw();
        		} else {
        			messageBox.innerHTML = '<p>' + mapfile + ' contained tile names not found in the atlas.</p>';
        			return;
        		}
        		
        		/*
        		 * This variable is true if a saved background map was loaded at the start
        		 * of the battle. At this point in the program, the loading has completed and
        		 * the turn controller can start.
        		 */
        		if(startDelay){
        			turnController = new TurnController(units);            
                    turnController.startTurn(currentUnits, true);
        		}
        		
        		// window.localStorage.setItem('background', mapfile);
        		
        	});
        	xhr.addEventListener('error', function(event){
        		messageBox.innerHTML = '<p>Could not download ' + mapfile + '</p>';
        		window.localStorage.removeItem('background', mapfile);
        	});
        	xhr.send();
        };
        
        /**
         * Check if this position is occupied
         * @this Battlemap.Background
         * @function Battlemap.Background#load
         * @param {number} dataIndex The position in the background to check
         * @returns {boolean} Whether or not this position is already occupied
         */
        Background.prototype.occupied = function(dataIndex){
            if(this.data[dataIndex] instanceof Array && this.data[dataIndex].length !== 0){
                return this.data[dataIndex];
            } else{
                return false;
            }
        };
        
        /**
         * <p>Draws all of the layers in the positions within the data property.<p>
         * 
         * <p>To start, draw the player castle somewhere on the top row and the
         * enemy castle somewhere on the bottom row. Everything else is plains.
         * "spt" is the sprite, or atlas frame, or tile, currently being drawn. The sprite
         * to be drawn is determined by the following logic:</p>
         * <ol>	
         * 	<li>On the top row, one of the tiles will be the enemy castle</li>
         *  <li>On the bottom row, one of the tiles will be the player castle</li>
         *  <li>Add the Castles and Layers to the Background</li>
         *  <li>Any remaining unoccupied tiles will have one of the plains sprites</li>
         * </ol>
         * 
         * @this Battlemap.Background
         * @function Battlemap.Background#draw
         * @param {string} type If a source is not specified, draw the map dynamically based on this type
         * 
         */
        Background.prototype.draw = function(type) {                
        	var thisBackground = this;
        	this.castles = new Castles();
        	castles = this.castles;
        	if(type === "random"){
                
                /*
                Add all the level 0 tiles to the background first
                */
                this.roughTerrain = new RoughTerrain(5, 3);
                this.roughTerrain.addToBackground(this);
                
                this.water = new Water(2, 1);
                this.water.addToBackground(this);
                
                var plains = (new Plains()).resourceSpts;
                
                /*
                 * If Array.prototype.forEach is used here, the browser may still
                 * execute the code after this block before going through the loop.
                 * Strangely, the same thing happens with a for-in loop. Only the
                 * old-fashioned way insures that all remaining non-filled tiles
                 * are given a plain before other code is executed.
                 */  
                for(var i = 0; i < this.data.length; i++){
                	if(!this.occupied(i)){// Put a random plain in the nonoccupied spaces.
                        var plainSpt = plains[Math.floor(Math.random() * plains.length)];
                        this.add(plainSpt, i);
                    }
                }
                
                
                /*
                 * Next, add all of the level 1 tiles to the background
                 */
                
                this.castles.addToBackground(this);                
                
                this.resources = new Resources(5, 3);
                this.resources.addToBackground(this);
                
                this.villages = new Villages(5, 3);
                this.villages.addToBackground(this);
                
                this.trees = new Trees(3, 2);
                this.trees.addToBackground(this);

                /*
                 * Now that the Castles and Layers have been put in this Background,
                 * go through this Background's data array and draw all the tiles
                 */
                for(var i = 0; i < this.data.length; i++){
                    // Draw each tile in this square
                    this.data[i].forEach(function(element, index){
                    	 var gridX = i % gridWidth;
                         var gridY = Math.floor(i / gridHeight);
                         
                         ctx.drawImage(atlasPng, element.frame.x, element.frame.y, element.frame.w, element.frame.h, gridX*tileWidth + gridXOffset, gridY*tileHeight + gridYOffset, tileWidth, tileHeight);
                    });
                }
            } else {
                /*
                If the drawing style is not random, expect a defined map either saved
                in this Background's data array or loaded through JSON at the
                src URL
                */
            	
                this.data.forEach(function(dataElement, index){
                    if(dataElement instanceof Array && dataElement.length !== 0){
                        dataElement.forEach(function(spt){
                            var gridX = index % gridWidth;
                            var gridY = Math.floor(index / gridHeight);
                            
                            /*
                             * Load the castle data in the the background's castle object
                             * so that the units can be placed appropriately.
                             */
                            if(spt.type === 'castle'){
                        		if(spt.description.match(/enemy/)){
                        			thisBackground.castles.enemy.column = gridX;
                        			thisBackground.castles.enemy.dataIndex = index;
                        		} else if(spt.description.match(/Your/)){
                        			thisBackground.castles.player.column = gridX;
                        			thisBackground.castles.player.dataIndex = index;
                        		}
                        	}
                            
                            ctx.drawImage(atlasPng, spt.frame.x, spt.frame.y, spt.frame.w, spt.frame.h, gridX*tileWidth + gridXOffset, gridY*tileHeight + gridYOffset, tileWidth, tileHeight);
                        });
                    }
                });
                
            }
        };
        
        /**
         * Redraw the canvas
         * 
         * @param {string} map The map to be redrawn, either 'random'
         * or the path to a defined one
         * @this Battlemap.Background
         * @function Battlemap.Background#redraw
         */
        Background.prototype.redraw = function(map){
        	ctx.clearRect(0, 0, canvas.width, canvas.height);
        	if(map){
        		this.draw(map);
        	} else {
        		this.draw();
        	}
        };
        
       /**
        * Make a new unit to be moved on the tactical battlefield
        * 
        * @this Battlemap.Token
        * @constructor
        * @memberof Battlemap
        * @param {string} player The affiliation of the unit.
        * @param {string} type The unit type of this token
        * @classdesc A unit that can be moved. Tokens are created in the Units constructor.
        */ 
       function Token(player, type){
    	   /** This token's affliation 
    	    * @member {string}
    	    * */
    	   this.player = player;
    	   /** The type of unit, e.g., squire, mage, archer, etc. 
    	    * @member {string}
    	    * */
    	   this.type = type;
    	   /** The sprite data for this token based on the value of Frames.frametype
    	    * @member {object}
    	    * */
    	   this.spt = this.resourceSpts[this.type];
    	   
    	   /** The current sprite / frame position this unit is assuming. This object is based on the
    	    * properties of the <code>frames</code> object in <code>atlas.json</code>. 
    	    * @member {object}
    	    * */
    	   this.curSpt = this.resourceSpts[this.type].idle[0];
    	   
           /** Whether or not the token is currently moving 
            * @member {boolean}
            * */
    	   this.moving = false;
    	   /** Whether or not the unit has moved 
    	    * @member {boolean}
    	    * */
    	   this.moved = false;
    	   /** Whether or not the unit has performed an action 
    	    * @member {boolean}
    	    * */
    	   this.actioned = true;
    	   
    	   /** Whether or not the unit has ended their turn 
    	    * @member {boolean}
    	    * */
    	   this.ended = false;
    	   
    	   /** The list of Command instances that have been issued for this token. Commands can be rolled back by removing
    	    * them from the list and calling their cancel method 
    	    * @member {array}
    	    * */
       	   this.commandQueue = [];
       	/** The list of commands that are available for the token. Its contents are used to
       	 * populate the UnitMenu 
       	 * @member {array}
       	 * **/
    	   this.commands = [];
    	   /** The current location of this token in the grid 
    	    * @member {number}
    	    * */
           this.dataIndex = null;    	   
    	   /** The previous location of this token in the grid 
    	    * @member {number}
    	    * */
           this.prevDataIndex = null;
    	   
           /** The destination data index of a token moving along the y-axis 
            * @member {number}
            * */
    	   this.yDataIndex;
    	   /** The number of tiles along the y-axis a unit is moving 
    	    * @member {number}
    	    * */
    	   this.yDiff;
    	   
    	   /** An array of the data indexes in this token's movement range 
    	    * @member {array}
    	    * */
    	   this.indexesInRange = [];
    	   /** A unit can move this many tiles out from where they are standing 
    	    * @member {number}
    	    * */
    	   this.range = 1;
    	   
    	   /* Movement range is based on unit type. */
    	   switch(this.type){
    	   	case 'pit':
    	   	case 'bandit':
    	   		this.range = 2;
    	   	break;
    	   }
    	   
    	   /**
            * Various stat parameters for the unit. The default values are in the
            * Grand Wars Units Status Sheet.xlsx file.
            * 
            * @member {object}
            */
    	   this.stats = {
   	   			"HP": '',
   	   			"MP": '',
   	   			"Attack": '',
   	   			"Intelligence": '',
   	   			"Defense": '',
   	   			"Magic Defense": '',
   	   			"Agility" : ''
           };
    	   
    	   /*
    	    * In the future, we might define a way for tokens to recall which Units
    	    * object they belong to.
    	    */
    	   var faction = this.player === 'player' ? 'roric' : 'red blades';
    	   
    	   /*
    	    * Initialize the stats for each Token based on their faction and unit type
    	    */
    	   for(var stat in this.stats){
    		   var statValue, level;
    		   if(!Token.levelStatMap.classLevelMap[faction][this.type]){
    			   var level = "A";    			   
    		   } else {
    			   var level = Token.levelStatMap.classLevelMap[faction][this.type][stat];    			   
    		   }
    		   var statValue = Token.levelStatMap.levels[level][stat];
    		   this.stats[stat] = statValue;
    	   }
       }
       
       /**
        * The sprites that represent the token on the battlefield
        * 
        * @this Battlemap.Token
        * @member {object}
        * @function Battlemap.Token#resourceSpts
        */
       Token.prototype.resourceSpts = {};
       for(var type in framesByType){
    	   Token.prototype.resourceSpts[type] = framesByType[type]; 
       }       
       
       /**
        * <p>A static object of the Token class that stores the mapping of Aptituide Level names
        * with default stat values. This object could be initialized from an ajax call in the future 
        * so that the values could be obtained from a database or file.</p>
        * 
        * <p>This object will have a load property, which is a function that loads the initial values
        * for this object. It will also have a levels property which points to an object whose properties 
        * are the uppercase names of Aptitude Level. These refer to objects that contain properties 
        * that map to the stat values for the Aptitude Level.</p>
        * 
        * @member Battlemap.Token.levelStatMap
        */
       Token.levelStatMap = {};
       
       /**
        * Load the levelStatMap for the first time.
        * 
        * @function Battlemap.Token.levelStatMap.load
        */
       Token.levelStatMap.load = function(){
    	   /**
    	    * Type in the values from the spreadsheet for now. In the future, those values will
    	    * be in a database that is read in.
    	    */
    	   this.levels = {
    			    	"SSS": {
      						"HP": 100,
      						"MP": 80,
      						"Attack": 50,
      						"Intelligence": 50,
      						"Defense": 25,
      						"Magic Defense": 45,
      						"Agility": "Godspeed"
      					},
      					"SS": {
      						"HP": 80,
      						"MP": 65,
      						"Attack": 45,
      						"Intelligence": 45,
      						"Defense": 20,
      						"Magic Defense": 45,
      						"Agility": 8
      					},
      					"S": {
      						"HP": 65,
      						"MP": 50,
      						"Attack": 40,
      						"Intelligence": 40,
      						"Defense": 15,
      						"Magic Defense": 30,
      						"Agility": 7
      					},
      					"A": {
      						"HP": 55,
      						"MP": 40,
      						"Attack": 30,
      						"Intelligence": 30,
      						"Defense": 14,
      						"Magic Defense": 25,
      						"Agility": 6
      					},
      					"B": {
      						"HP": 40,
      						"MP": 30,
      						"Attack": 25,
      						"Intelligence": 25,
      						"Defense": 12,
      						"Magic Defense": 20,
      						"Agility": 5
      					},
      					"C": {
      						"HP": 30,
      						"MP": 20,
      						"Attack": 20,
      						"Intelligence": 20,
      						"Defense": 10,
      						"Magic Defense": 15,
      						"Agility": 4
      					},
      					"D": {
      						"HP": 20,
      						"MP": 15,
      						"Attack": 15,
      						"Intelligence": 15,
      						"Defense": 8,
      						"Magic Defense": 10,
      						"Agility": 3
      					},
      					"E": {
      						"HP": 15,
      						"MP": 10,
      						"Attack": 10,
      						"Intelligence": 10,
      						"Defense": 5,
      						"Magic Defense": 5,
      						"Agility": 2
      					},
      					"F": {
      						"HP": 10,
      						"MP": 5,
      						"Attack": 5,
      						"Intelligence": 5,
      						"Defense": 3,
      						"Magic Defense": 3,
      						"Agility": 1
      					}
    	    }
    	   
    	   this.classLevelMap = {
        		   "roric":{
    				   "chevalier": {
    	    			   "HP": "E",
    	    			   "MP": "F",
    	    			   "Attack": "E",
    	    			   "Intelligence": "F",
    	    			   "Defense": "C",
    	    			   "Magic Defense": "D",
    	    			   "Agility": "E"
    	       		   },
    	           	   "fencer": {
    	     			   "HP": "F",
    	     			   "MP": "E",
    	     			   "Attack": "E",
    	     			   "Intelligence": "F",
    	     			   "Defense": "F",
    	     			   "Magic Defense": "E",
    	     			   "Agility": "C"
    	        	   }
        		   },
        		   "red blades":{
        			   "fighter": {
        				   "HP": "C",
    	    			   "MP": "F",
    	    			   "Attack": "B",
    	    			   "Intelligence": "F",
    	    			   "Defense": "C",
    	    			   "Magic Defense": "E",
    	    			   "Agility": "E"
        			   },
        			   "bandit": {
        				   "HP": "C",
    	    			   "MP": "F",
    	    			   "Attack": "D",
    	    			   "Intelligence": "F",
    	    			   "Defense": "C",
    	    			   "Magic Defense": "C",
    	    			   "Agility": "F"
        			   }
        		   }
           };
       };
       
       /**
        * @function Battlemap.Token.levelStatMap.getStat
        * @this Battlemap.Token.levelStatMap
        * 
        * @param {string} The aptitude level for the stat value to get
        * @param {string} The type of stat to get
        */
       Token.levelStatMap.getStat = function(level, stat){
    	   return this.levels[level][stat];
       };
       
       /**
        * A map of the unit types for factions and the aptitiude levels for their starting stats.
        * 
        * @member {object} Battlemap.Token.classLevelMap
        */
       Token.levelStatMap.classLevelMap = {};
       
       /**
        * If the unit has both actioned and moved, it is not available.
        * @this Battlemap.Token
        * @function Battlemap.Token#isAvailable
        * @returns {boolean}
        */
       Token.prototype.isAvailable = function(){
    	 return !this.ended;  
       };
       
       /**
        * Tests whether or not the token can be moved in the direction
        * specified for the length of a single tile.
        * 
        * @this Battlemap.Token
        * @function Battlemap.Token#canMovePiece
        * @param {number} dataIndex The current position of the Token in the
        *                           Units data array.
        * @param {string} direction The key pressed indicating the direction of movement
        * @return {boolean}
        */
       Token.prototype.canMovePiece = function(dataIndex, direction){
    	   switch(direction){
	           case 'Up':
	               if(dataIndex < gridHeight){
	                   // We're on the top row, so we can't move up.
	                   return false;
	               }
	           break;
	           case 'Left':
	               if(dataIndex % gridWidth === 0){
	                   // We're in the left most column, so we can't move left.
	                   return false;
	               }
	           break;
	           case 'Down':
	               if((dataIndex + gridHeight) >= background.data.length){
	                   // We're on the bottom row, so we can't move down.
	                   return false;
	               }
	           break;
	           case 'Right':
	               if((dataIndex % gridWidth) === (gridWidth - 1)){
	                   // We're in the right most column, so we can't move right.
	                   return false;
	               }
	           break;
	           default:
	        	   return false;
	           break;
    	   }
    	   
    	   return true;
       };
       /**
		Moves a piece along the x and y difference specified. A zero value
		for the x or y difference means the piece stays on that axis while moving.
		
		@this Battlemap.Token
		@function Battlemap.Token#movePiece
		@param {number} newDataIndex The new data index the unit will be at after the move
		@param {number} xDiff The number of squares left or right (positive or negative)
		the piece is being moved.
		@param {number} yDiff The number of squares up or down (positive or negative)
		the piece is being moved.
		@param {string} direction The direction the unit is being moved.
		*/
       Token.prototype.movePiece = function (newDataIndex, xDiff, yDiff, direction){
    	   /*
    	    * When a token starts moving, make sure the cursor handlers
    	    * are removed.
    	    */
    	   cursor.removeHandlers();
    	   
    	   if(this.moving){
               return;
           }
    	   /*
           If it previously was not moving and is now being moved,
           update the token's state.
           */
    	   this.moving = true;
    	   
    	   if(!direction){
    		   if(xDiff === 0 && yDiff === 0){
    			   return;
    		   }
    		   
    		   if(xDiff !== 0 && yDiff !== 0){
    			   Units.activeToken.yDiff = yDiff;
    			   yDiff = 0;
    		   } else if (xDiff === 0 && yDiff !== 0){
    			   yDiff = Units.activeToken.yDiff;
    			   Units.activeToken.yDiff = 0;
    		   }
    		   
    		   if(xDiff > 0){
    			   direction = 'Right';
    		   } else if(xDiff !== 0 && yDiff === 0){
    			   direction = 'Left';
    		   } else if(yDiff > 0){
    			   direction = 'Down';
    		   } else if(yDiff !== 0){
    			   direction = 'Up';
    		   }
    	   }
    	   var token = this;
           
           /* 
           Find the current grid coordinates of the unit 
           */
           var currentGrid = indexToCoordinates(Units.activeToken.dataIndex);
           
           var curGridX = currentGrid.gridX;
           var curGridY = currentGrid.gridY;
           
           /*
           Find the pixel coordinates of the unit
           */
           var curPosX = currentGrid.posX;
           var curPosY = currentGrid.posY;
           
           /*
           Find out the new grid coordinates after the move
           */
           var newGrid = indexToCoordinates(newDataIndex);
           
           var newGridX = newGrid.gridX;
           var newGridY = newGrid.gridY;
           
           /*
           Determine the new pixel coordinates of the piece after moving
           */
           var newPosX = newGrid.posX;
           var newPosY = newGrid.posY;
           
           var start = null;
           var position = 0;
           /**
           Draw the piece's new position. This is called at the 
           browser's requestFrameAnimation rate, which is either 60 frames
           a second or the monitor's refresh rate.
           
           @param {DOMHighResTimeStamp} timestamp
           */
           function drawFrame(timestamp){
               	var progress;
               	if (start === null){
               		start = timestamp;
               	}
               	progress = timestamp - start;
               	
                if((curPosX === newPosX && (xDiff !== 0)) || 
                       (curPosY === newPosY && (yDiff !== 0))){
                       /* 
	                   The piece has reached the new destination and 
	                   the animation should end.
	                   
	                   First, record the token's new position
	                   */
                	   var originalIndex = token.dataIndex;
	            	   token.dataIndex = newDataIndex;
	                   // Change it to the not-moving state
	            	   token.moving = false;		               
	               	
		               // Clear the canvas, redraw the background, and draw the unit at their final position
	            	   background.redraw();
		               
		               var dWidth, dx, endSpt = token.spt.idle[0];
		               if(endSpt.frame.w < tileWidth){
		               	dx = curPosX + (tileWidth - endSpt.frame.w) / 2;
		               	dWidth = endSpt.frame.w; 
		               } else {
		               	dx = curPosX;
		               	dWidth = tileWidth;
		               }
		               ctx.drawImage(atlasPng, endSpt.frame.x, endSpt.frame.y, endSpt.frame.w, endSpt.frame.h, dx, curPosY, dWidth, tileHeight);
		               // Make sure the other tokens are drawn as well
		               var skipActive = true;
		               units.draw(skipActive);
		               
		               /*
		                * After moving the token in the xDiff direction, move it in 
		                * the saved yDiff direction.
		                */
		               if(Units.activeToken.yDiff !== 0){
		            	   Units.activeToken.movePiece(Units.activeToken.yDataIndex, 0, Units.activeToken.yDiff);
		               } else {
		            	   // The token has reached its final destination
		            	   // Remove the token from currentUnits.data
			               if(currentUnits.data[token.prevDataIndex] instanceof Array){
			                   currentUnits.data[token.prevDataIndex].forEach(function(element, index, array){
			                       if(element === token){
			                           array.splice(index, 1);
			                           return;
			                       }
			                   });
			               }
			               
		            	   /*
			                * Put this token in its new location in the Units data array
			                */
			               if(currentUnits.data[newDataIndex] instanceof Array){
			                   currentUnits.data[newDataIndex].push(token);
			               } else {
			                   currentUnits.data[newDataIndex] = [token];
			               }
			               
			               if(currentUnits.player === 'player'){
			            	   units.teams[0] = currentUnits;
			               } else {
			            	   units.teams[1] = currentUnits;
			               }
			               
		            	   // Now, find out what is in the corresponding background data index
			               if(Units.activeToken instanceof Token){
			               	currentUnits.stateLocation();
			               }
		            	   
		            	   // The token should now be finished moving
		            	   token.moving = false;
		            	   token.moved = true;
		            	   
		            	   /*
		            	    * Now that we're done moving, show the unit menu again after adding
		            	    * the menu commands to the commandQueue. Remove the cursor handlers.
		            	    */
		            	   Units.activeToken.commandQueue.push(new Command('activeMenu', activeMenu.showMenu, activeMenu.hideMenu));
		            	   Units.activeToken.commandQueue[Units.activeToken.commandQueue.length - 1].start(Units.activeToken.type);
		            	   cursor.removeHandlers();
		            	   
		            	   activeMenu.activeCommand = '';
		            	   activeMenu.previousCommand = 'Move';
		               }						                           
                   } else {
                	   var rate;
                       if(direction === 'Left' || direction === 'Right'){
                    	   rate = Math.abs(xDiff);
                       } else {
                    	   rate = Math.abs(yDiff);
                       }
                	   
                	   // Move one pixel per drawRate
                       curPosX += xDiff / rate;
                       curPosY += yDiff / rate;
                       
                       // Clear the canvas and background.redraw the background
                       background.redraw();
                       
                       /*
                        * Cycle through the token's movement animations 
                        * that corresponds with the direction every 100 ms
                        */
                       
                       if(progress >= 100){
                       	if(position >= token.spt.walk[direction].length){
                       		position = 0;
                       	}
                       	token.curSpt = token.spt.walk[direction][position++];
                       	start = null;
                       }
                       
                       // Draw the token's next new position
                       var dWidth, dx;
                       if(token.curSpt.frame.w < tileWidth){
                       	dx = curPosX + (tileWidth - token.curSpt.frame.w) / 2;
                       	dWidth = token.curSpt.frame.w; 
                       } else {
                       	dx = curPosX;
                       	dWidth = tileWidth;
                       }
                       ctx.drawImage(atlasPng, token.curSpt.frame.x, token.curSpt.frame.y, token.curSpt.frame.w, token.curSpt.frame.h, dx, curPosY, dWidth, tileHeight);
                       
                       // Draw the other tokens in their current state while the active token is moving
                       var skipActive = true;
                       units.draw(skipActive);
                       
                       window.requestAnimationFrame(drawFrame);
                   }
               }
           		
           	   window.requestAnimationFrame(drawFrame);
           };
        /**
         * Highlight the tiles that can be moved to based on the token's range.
         * 
         * @this Battlemap.Token
         * @function Battlemap.Token#showMoveRange
         * @param {boolean} find If this is true, find the move range but don't show it.
         * @param {number} range Test a movement range to fill activeToken.indexesInRange.
         * 						 
         */
        Token.prototype.showMoveRange = function(find, range){
        	Units.activeToken.indexesInRange = [];
        	
        	
        	range = range ? range : Units.activeToken.range;
        	var token = Units.activeToken;
            
        	if(!find){
        		ctx.save();
                ctx.fillStyle = 'rgba(0,0,255,0.4)'; // The move range highlight color
        	}
            
            
            var level = 1;
            /**
             * Determines which tiles to highlight based on token.range. The token's position in
             * the Units data array is passed as the reference point from which to determine
             * the tiles that can be moved to. Starting at that position, checkUp and checkDown
             * find out whether or not the tiles above and below the token can be highlighted
             * for movement. 
             * 
             * Next, the tiles to the right of the token's current position are checked for 
             * movement access. If the position can be moved to, that tile is highlighted and
             * checkUp and checkDown are called from that position as well. The level variable
             * tracks the level of range that has been checked left and right from the token's 
             * current position. 
             * 
             * @param {number} dataIndex The token's position in the Units data array.
             * @function Battlemap.Token~checkRange
             */
            function checkRange(dataIndex){
            	/**
            	 * Highlights tiles above the token that can be moved to
            	 * 
            	 * @param {string} position The index in the Units data array that in the origin of the
            	 * 				   top and bottom range to check.
            	 * @function Battlemap.Token~checkUp
            	 */
            	function checkUp(position){
            		var tempposition = position - gridHeight;
            		if(token.canMovePiece(position, 'Up') && lvl <= range){
            			lvl++;
            			
            			/* 
                        Find out the grid coordinates of the unit from its index
                        in the data array.
                        */
                        var gridX = tempposition % gridWidth;
                        var gridY = Math.floor(tempposition / gridHeight);
                        
                        if((currentUnits.data[tempposition] === undefined || currentUnits.data[tempposition].length === 0)){
                        	if(!find){
                        		ctx.fillRect(gridX*tileWidth + gridXOffset, gridY*tileHeight + gridYOffset, tileWidth, tileHeight);
                        	}                        	
                        	token.indexesInRange.push(tempposition);
                        }            			
                        
            			checkUp(tempposition);
            		} else {
            			lvl = 1;
            			return;
            		}
            	}
            	
            	/**
            	 * Highlights tiles above the token that can be moved to
            	 * 
            	 * @param {string} The index in the Units data array that in the origin of the
            	 * 				   top and bottom range to check.
            	 * @function Battlemap.Token~checkDown
            	 */
            	function checkDown(position){
            		var tempposition = position + gridHeight;
            		if(token.canMovePiece(position, 'Down') && lvl <= range){
            			lvl++;            			
            			
            			/* 
                        Find out the grid coordinates of the unit from its index
                        in the data array.
                        */
                        var gridX = tempposition % gridWidth;
                        var gridY = Math.floor(tempposition / gridHeight);
                        
                        if((currentUnits.data[tempposition] === undefined || currentUnits.data[tempposition].length === 0)){
                        	if(!find){
                        		ctx.fillRect(gridX*tileWidth + gridXOffset, gridY*tileHeight + gridYOffset, tileWidth, tileHeight);
                        	}
                        	token.indexesInRange.push(tempposition);
                        }
                        
            			checkDown(tempposition);
            		} else {
            			lvl = 1;
            			return;
            		}
            	}
            	
            	var lvl = 1, newposition;
            	
        		checkUp(dataIndex);
        		checkDown(dataIndex);

        		newposition = dataIndex + 1;
            	while(level <= range){
            		if(token.canMovePiece(newposition - 1, 'Right')){
            			
            			/* 
                        Find out the grid coordinates of the unit from its index
                        in the data array.
                        */
                        var gridX = newposition % gridWidth;
                        var gridY = Math.floor(newposition / gridHeight);
                        
                        if((currentUnits.data[newposition] === undefined || currentUnits.data[newposition].length === 0)){
                        	if(!find){
                        		ctx.fillRect(gridX*tileWidth + gridXOffset, gridY*tileHeight + gridYOffset, tileWidth, tileHeight);
                        	}
                        	token.indexesInRange.push(newposition);
                        }
                        
            			checkUp(newposition);
            			checkDown(newposition);
            		} else {
            			break;
            		}
            		newposition = newposition + 1;
            		level++;
            	}
            	
            	level = 1;
            	
            	newposition = dataIndex - 1;
            	while(level <= range){
            		if(token.canMovePiece(newposition + 1, 'Left')){
            			
            			/* 
                        Find out the grid coordinates of the unit from its index
                        in the data array.
                        */
                        var gridX = newposition % gridWidth;
                        var gridY = Math.floor(newposition / gridHeight);
                        
                        if((currentUnits.data[newposition] === undefined || currentUnits.data[newposition].length === 0)){
                        	if(!find){
                        		ctx.fillRect(gridX*tileWidth + gridXOffset, gridY*tileHeight + gridYOffset, tileWidth, tileHeight);
                        	}
                        	token.indexesInRange.push(newposition);
                        }
            			checkUp(newposition);
            			checkDown(newposition);
            		} else {
            			break;
            		}
            		newposition = newposition - 1;
            		level++;
            	}
            }
            
            checkRange(this.dataIndex);
            ctx.restore();
        };
        
        /**
         * Initiate token movement. Add the default and move handlers
         * for the cursor. If a move is being redone, Units.activeToken.prevDataIndex
         * should not be null.
         * 
         * @this Battlemap.Command
         * @function Battlemap.Token#moveCommand
         */
        Token.prototype.moveCommand = function(){
        	activeMenu.activeCommand = 'Move';
        	if(Units.activeToken.moved === true){
        		return;
        		activeMenu.activeCommand = '';
        	}
        	
        	cursor.readyHandlers();
        	        	
        	cursor.readyMoveHandlers();
        	if(Units.activeToken.prevDataIndex !== null ){
        		currentUnits.data.splice(Units.activeToken.dataIndex, 1, undefined);
        		if(currentUnits.data[Units.activeToken.prevDataIndex] instanceof Array){
        			currentUnits.data[Units.activeToken.prevDataIndex].push(Units.activeToken);
        		} else {
        			currentUnits.data[Units.activeToken.prevDataIndex] = [Units.activeToken];
        		}
        		Units.activeToken.dataIndex = Units.activeToken.prevDataIndex; 
        	} else {
        		Units.activeToken.prevDataIndex = Units.activeToken.dataIndex;
        	}
        	background.redraw();
        	cursor.draw(Units.activeToken.dataIndex);

        };
        
        /**
         * End the unit's turn
         * @this Battlemap.Command
         * @function Battlemap.Token#endCommand
         */
        Token.prototype.endCommand = function(){
        	Units.activeToken.ended = true;
        	background.redraw();
        	units.draw();
        	Units.activeToken.prevDataIndex = null;
        	Units.activeToken.commandQueue = [];
        	turnController.nextUnit();
        	activeMenu.activeCommand = '';
        };
        
        /**
        Create new Units for a team
        
        @this Battlemap.Units
        @memberof Battlemap
        @constructor
        @param {string} player The affiliation of the units. The default is "player"
        @param {string} faction The display name of the faction the units belong to
        @classdesc Sets up the units to be present on the battlefield. Their positions are
        also recorded in a data array that represents every grid location.
        */
        function Units(player, faction){
        	/**
        	 * @member {array} Battlemap.Units#tokens A list of tokens that this Units object represents
        	 */
        	
            if(!player){
                player = "player";
            }
			
            /** 
             * 'enemy' or 'player'
             * @member {string}
            */
            this.player = player;
            
            /**
             * The faction the units belong to
             * @member {string}
             */
            this.faction = faction;            
            
            /**
             * An array of tokens, or units, on the battlefield. Assign them based on
             * the player value sent
             */
            switch(faction){
            	case 'roric':
            		this.tokens = [
            		               new Token(this.player, 'marketh'), 
            		               new Token(this.player, 'pit'),
            		               new Token(this.player, 'fencer'),
            		               new Token(this.player, 'chevalier')
            		];
            	break;
            	case 'red blades':
            		this.tokens = [
            		               new Token(this.player, 'dragon'), 
            		               new Token(this.player, 'bandit'),
            		               new Token(this.player, 'fighter'),
            		];
            	break;
            }                        
            
            /**
             * The data array for units that represents their location
             * on the battlefield grid
             */
            this.data = new Array(gridWidth * gridHeight);
        }
        
        /** The list of Command instances that have been issued for this team of units. Commands can be rolled back by removing
 	    * them from the list and calling their cancel method 
 	    * @member {array}
 	    * */
        Units.commandQueue = [];
        
        /**
         * The token that is currently active and can be moved
         * by the user
         * @member {object} Battlemap.Units.activeToken
         */
        Units.activeToken = {};
        /**
         * Updates the descriptionBox with a description of the tile
         * the token is currently on.
         * 
         * @this Battlemap.Units
         * @function Battlemap.Units#stateLocation
         */
        Units.prototype.stateLocation = function(){
			var backgroundTile;
			backgroundTile = background.data[Units.activeToken.dataIndex];
			
			descriptionBox.innerHTML = backgroundTile[0].description;
			typeBox.innerHTML = backgroundTile[0].type;
		};
		
		/**
		 * Draw the units for the first time.
		 * 
		 * @this Battlemap.Units
		 * @function Battlemap.Units#init
		 * @todo Find out why units.occupied() doesn't pick up on Tokens added to
		 * 		 this Units.data.
		 */
		Units.prototype.init = function(){
			var spt, dataIndex, theUnits = this;
            
            this.tokens.forEach(function(element, index, array){
            	if(index === 0){
            		/*
                    Make sure the player or enemy token starts at 
                    the corresponding castle.
                    */
                    spt = element.spt;
                    dataIndex = castles[theUnits.player].dataIndex;
                    element.dataIndex = dataIndex;
            	} else {
            		/*
                     * The other units will go on the same row as the castle.
                     */
                    var prevIndexes = [];
                    var offset = theUnits.player === 'player' ? 210 : 0;
                    var newIndex = Math.floor(Math.random() * 15 + offset);
                    
                    // Units added to the map aren't showing up at this check
                    while(units.occupied(newIndex)){
                    	newIndex = Math.floor(Math.random() * 15 + offset);
                    }
                    
                    dataIndex = newIndex;
                    element.dataIndex = dataIndex;
            	}
                
            	/*
                Record this tile's location in the data array
                */  
                if(theUnits.data[dataIndex] instanceof Array){
                	theUnits.data[dataIndex].push(element);
                } else{
                	theUnits.data[dataIndex] = [element];
                }
            });
            
            this.draw();
		};
		
		/**
        * Draw the units in their current state.
        @this Battlemap.Units
        @param {boolean} skipActive Don't draw the active token. This will be set to true to redraw the other units
        							during the active token's movement animation.
        @function Battlemap.Units#draw
        */
		Units.prototype.draw = function(skipActive){
            var gridX, gridY, dataIndex, spt;
            
            if(!activeMenu) activeMenu = new UnitMenu();
            
            this.tokens.forEach(function(element, index, array){
            	if(!(skipActive && element === Units.activeToken)){
            		var stance;
                	if(element.ended){
                		stance = 'end';
                	} else if(activeMenu.activeCommand === 'showMenu' && element === Units.activeToken){
                		stance = 'attention';
                	} else {
                		stance = 'idle';
                	}
                    spt = element.spt[stance][0];
                    dataIndex = element.dataIndex;                                			
        			
                    /* 
                    Find out the grid coordinates of the unit from its index
                    in the data array.
                    */
                    var gridX = dataIndex % gridWidth;
                    var gridY = Math.floor(dataIndex / gridHeight);
                    
                    /*
                    Use the grid coordinates to find out the pixel coordinates to draw it to on the canvas
                    */
                    var dWidth, dx;
                    if(spt.frame.w < tileWidth){
                    	dx = (gridX*tileWidth + gridXOffset) + (tileWidth - spt.frame.w) / 2;
                    	dWidth = spt.frame.w; 
                    } else {
                    	dx = gridX*tileWidth + gridXOffset;
                    	dWidth = tileWidth;
                    }
                    ctx.drawImage(atlasPng, spt.frame.x, spt.frame.y, spt.frame.w, spt.frame.h, dx, gridY*tileHeight + gridYOffset, dWidth, tileHeight);
            	}
            	
            });
            
            if(Units.activeToken instanceof Token){
            	currentUnits.stateLocation();
            }
            
            if(activeMenu.activeCommand === 'Move' && !skipActive && this === currentUnits){
            	Units.activeToken.showMoveRange();
            }
        };
        
        /** 
         * Create a new cursor for the battlefield
         * 
         * @constructor
         * @memberof Battlemap
         * @this Battlemap.Cursor
         * @classdesc The battlefield's cursor, used to select a tile and interact with its contents.
         * The handlers for the cursor are properties of <code>this</code> within the constructor so
         * that a reference to the cursor object can be maintained while the handler
         * functions are made into event listeners, where <code>this</code> becomes the HTML element that the
         * handler was added to. 
         */
        function Cursor(){
        	/**
        	 *  @member {number} Battlemap.Cursor#dataIndex The current index of the cursor in the battlefield grid. It starts
        	 *  at the first column of the last row. 
        	 */
        	
        	var dataIndex = this.dataIndex;
        	var cursor = this;
        	
        	if(this.dataIndex == null){
        		/**
        		 * The index in the battlefield grid where the cursor currently resides. It starts at the
        		 * first column of the last row.
        		 *  @member {number} 
        		 */
            	this.dataIndex = gridWidth * (gridHeight - 1);
        	}
        	
        	/** The handlers currently registered for the cursor. This array will
        	 * be passed to {@link Cursor.removehandlers#|removeHandlers()} 
        	 * @member {array}
        	 * */
        	this.registeredHandlers = [];
        	
        	/**
        	 * Moves the cursor a single tile in the direction specified by the key
        	 * pressed.
        	 * 
        	 * @param {Event} event Expecting a KeyboardEvent
        	 */
        	this.moveHandler = function(event){
        		event.preventDefault();
            	if(cursor.dataIndex == null){
            		cursor.dataIndex = gridWidth * (gridHeight - 1);
            	}
            	
            	// Conform the name of the key pressed
            	eventKey = '';
            	if(event.key){
            		eventKey = bindings.filterEventKey(event.key);
            	}
            	
            	// Only register the key if it is recognized
            	if(!(eventKey in bindings || event.keyCode in bindings)){
            		return;
            	}
            	var direction = eventKey ? bindings[eventKey].command : bindings[event.keyCode].command;
            	
            	if(cursor.move(direction) === false) return;
            	
            	if(!cursorInfo.classList.contains('closed')){
            		cursor.stateLocation(); 
            		
            		var close = true;
        			for(var team in units.teams){
                		if((Array.isArray(units.teams[team].data[cursor.dataIndex]) && units.teams[team].data[cursor.dataIndex].length !== 0)){
                			cursor.stateUnitInfo();
                			close = false;
                		}                    		
        			}
        			
        			if(close){
        				unitInfo.classList.add('closed');
        			} else {
        				unitInfo.classList.remove('closed');
        			}
            	}
            };
        	
            /**
             * Handler for selecting which tile to move the unit to
             * @param {Event} event The key event
             */
            this.unitKeyMove = function(event){
        		if(Units.activeToken.moving || event.keyCode !== bindings.select.keyCode){
        			return; // Ignore input if the token is already moving
        		}
        		
        		cursor.removeHandlers();
        		
        		var grid = indexToCoordinates(cursor.dataIndex);
        		var scrollOffsets = getScrollOffsets();
        		var x = grid.posX - canvasContainer.offsetLeft + scrollOffsets.x;
            	var y = grid.posY - canvasContainer.offsetTop + scrollOffsets.y;
            	
            	var gridX = grid.gridX;
            	var gridY = grid.gridY;
            	
            	for(var index in Units.activeToken.indexesInRange){
            		var tempGridX = Units.activeToken.indexesInRange[index] % gridWidth;
                    var tempGridY = Math.floor(Units.activeToken.indexesInRange[index] / gridHeight);
                    
                    if(tempGridX === gridX && tempGridY === gridY){
                    	// User is clicking a tile that the token can move to
                    	
                    	// Find the token's current tile coordinates
                    	var currentX = Units.activeToken.dataIndex % gridWidth;
                    	var currentY = Math.floor(Units.activeToken.dataIndex / gridHeight);
                    	
                    	var xDiff = gridX - currentX;
                    	var yDiff = gridY - currentY;
                    	
                    	var xDataIndex = Units.activeToken.dataIndex + xDiff;
                    	var yDataIndex = xDataIndex + (gridHeight * yDiff);
                    	
                    	Units.activeToken.yDataIndex = yDataIndex;
                    	Units.activeToken.yDiff = yDiff;
                    	
                    	if(xDiff === 0){
                    		Units.activeToken.movePiece(yDataIndex, xDiff, yDiff);
                    	}else {
                    		Units.activeToken.movePiece(xDataIndex, xDiff, yDiff);
                    	}
                    	break; // Exit the loop
                    } else { 
                    	/*
                    	 * If user is selecting a tile out of movement range, just
                    	 * ready the default and move command cursor handlers again.
                    	 */
                    	
                    	cursor.readyHandlers();
                    	cursor.readyMoveHandlers();
                    }
            	}
        	};        	
        	
            /**
             * Shows info about the background and unit at this tile 
             * @param {Event} event Expecting a KeyboardEvent
             * @param {boolean} close Close the info box
             * @this HTMLCanvasElement|Battlemap.Cursor
             */
        	this.infoHandler = function(event, close){
            	if(event.keyCode === bindings.info.keyCode){ // i
            		
            		if(close){
            			cursorInfo.classList.add('closed');
            			unitInfo.classList.add('closed');
            		} else {
            			cursorInfo.classList.toggle('closed');
            			
            			if(!unitInfo.classList.contains('closed')){
            				/*
            				 * If it is not already closed, assume the user wants to close it. 
            				 */
            				unitInfo.classList.add('closed');
            			} else {
            				if(!cursorInfo.classList.contains('closed')){
            					
            				
	            				/*
	            				 * Otherwise, show the unitInfo box if a unit is in any teams array under units.
	            				 */
	            				for(var team in units.teams){
	                        		if((Array.isArray(units.teams[team].data[cursor.dataIndex]) && units.teams[team].data[cursor.dataIndex].length !== 0)){
	                        			unitInfo.classList.remove('closed');
	                        			break;
	                        		}                    		
	                			}
            				}
            			}
            		}
            		
            		if(!cursorInfo.classList.contains('closed')){
            			cursor.stateLocation();
            			cursor.stateUnitInfo();
            		} else {
            			/*
            			 * Clear out the contents of the cursorInfo and unitInfo boxes if they
            			 * are closed.
            			 */
            			Array.prototype.forEach.call(document.querySelectorAll('#cursorInfo .background *'), function(element){
            				element.parentNode.removeChild(element);
            			});
            			cursorInfo.removeAttribute('style');
            			
            			Array.prototype.forEach.call(document.querySelectorAll('#unitInfo *'), function(element){
            				element.parentNode.removeChild(element);
            			});
            			unitInfo.removeAttribute('style');
            		}
            	}
            };
            
            /**
             * Handles selecting and cancelling.
             * @param {Event} event The key event
             */
            this.execHandler = function (event){
            	if(event.keyCode === bindings.select.keyCode){ // z
            		switch(activeMenu.activeCommand){
            			case 'Move':
            			break;
            			default:
            					/*
            					 * The unit is selected with the cursor
            					 */
                        		var tile = currentUnits.data[cursor.dataIndex];
                        		var token;
                        		
                        		if(tile instanceof Array && tile.length > 0){
                        			if(tile[0] instanceof Token){
                        				token = tile[0];
                        				Units.activeToken = token;
                        				
                        				/*
                        				 * Track this unit in the TurnController's currentIndex
                        				 */
                        				currentUnits.tokens.every(function(element, index){
                                    		if(element === Units.activeToken){
                                    			turnController.currentIndex = index;
                                    			return false;
                                    		} else {
                                    			return true;
                                    		}
                                    	});
                        				
                        				// Don't select the token if its turn has ended
                        				if(!activeMenu.canShowMenu()){
                        					return;
                        				}
                        				
                        				/*
                        				 * Make showing the menu the next command and then start it
                        				 */
                        				activeMenu.addShowMenuCommand();
                        				activeMenu.startCommand();
                        			}
                        		}
            		}
            	} else if(event.keyCode === bindings.cancel.keyCode){ // x 
            		activeMenu.cancelCommand(true);
            	}
            };
            
            this.readyHandlers();
        }
        
        /**
         * Show information about the tiles at the cursor's location.
         * 
         * @this Battlemap.Cursor
         * @function Battlemap.Cursor#stateLocation
         */
        Cursor.prototype.stateLocation = function(){
        	var cursor = this;
        	var backgroundDiv = document.querySelector('#cursorInfo .background'); 
			
			backgroundDiv.innerHTML = '';
			
        	var backgroundFrames = background.data[this.dataIndex];
        	
        	backgroundFrames.forEach(function(element){
        		backgroundDiv.innerHTML += '<div>' + element.description + '</div>';
        	});
			
			var grid = indexToCoordinates(this.dataIndex);
            
            cursorInfo.style.left = (grid.posX - 109) + 'px';
            cursorInfo.style.top = grid.posY + tileHeight + 'px';            
        };
        
        /**
         * Show information about the unit in the square
         * 
         * @this Battlemap.Cursor
         * @function Battlemap.Cursor#stateUnitInfo
         */
        Cursor.prototype.stateUnitInfo = function(){
        	Array.prototype.forEach.call(document.querySelectorAll('#unitInfo *'), function(element){
				element.parentNode.removeChild(element);
			});
        	
        	for(var team in units.teams){
        		if(Array.isArray(units.teams[team].data[this.dataIndex])){
        			var currentTokenList = units.teams[team].data[this.dataIndex];
        			// The token under the cursor should be the first token in the list
        			var currentToken = currentTokenList[0]
        			
        			var faction = units.teams[team].faction;
        			faction = faction.replace(/\s/,'');
        			var crestFrame = atlas.frames[faction + '_crest.png'].frame;
        			
        			unitInfo.className = faction;
        			
        			var crestDiv = document.createElement('div');
        			crestDiv.className = 'crest';
        			crestDiv.style.backgroundPosition = '-' + crestFrame.x + 'px -' + crestFrame.y + 'px';
        			
        			var unitInfoHeader = document.createElement('h1');
        			unitInfoHeader.classList.add('unitType');
        			
                	var statsBlock = document.createElement('div');
                	statsBlock.classList.add('stats');
                	
                	unitInfo.appendChild(crestDiv);
                	unitInfo.appendChild(unitInfoHeader);
                	unitInfo.appendChild(statsBlock);
                	
                	unitInfoHeader.textContent = currentToken.type;
                	
                	var statsToShow = ["HP", "MP"];
                	
                	for(var index in statsToShow){
                		var statRow = document.createElement('div');
                		statRow.className = "statRow";
                		
                		var statLabel = document.createElement('span');
                		statLabel.className = "statLabel";
                		statLabel.textContent = statsToShow[index] + ":";
                		
                		var statbarContainer = document.createElement('div');
                		statbarContainer.className = "statbarContainer";
                		
                		var statbar = document.createElement('div');
                		statbar.className = "statbar";
                		statbar.textContent = currentToken.stats[statsToShow[index]];
                		
                		statbarContainer.appendChild(statbar);
                		
                		statRow.appendChild(statLabel);
                		statRow.appendChild(statbarContainer);
                		
                		statsBlock.appendChild(statRow);
                	}
        		}
        	}
        	
        	var grid = indexToCoordinates(this.dataIndex);
        	unitInfo.style.left = (grid.posX + tileWidth) + 'px';
        	unitInfo.style.top = (grid.posY - tileHeight) + 'px';
        };
        
        /**
         * Activate all default input listeners for the cursor 
         * 
         * @this Battlemap.Cursor
         * @function Battlemap.Cursor#readyHandlers
         */
        Cursor.prototype.readyHandlers = function(){
        	if(theAI.enabled){
        		return;
        	}
        	if(this.registeredHandlers.length === 0){
        		this.registeredHandlers = [
       				{eventType: 'keydown', handler: this.moveHandler},
       				{eventType: 'keydown', handler: this.execHandler},
       				{eventType: 'keydown', handler: this.infoHandler}
        		];
        	}
        	
        	for(var index in this.registeredHandlers){
        		canvas.addEventListener(this.registeredHandlers[index].eventType, 
        								this.registeredHandlers[index].handler);
        	}
        };
        
        /**
         * Remove all registered handlers for the cursor and clear the
         * handlers array
         * 
         * @this Battlemap.Cursor
         * @function Battlemap.Cursor#removeHandlers
         */
        Cursor.prototype.removeHandlers = function(){
        	for(var i in this.registeredHandlers){
        		if(this.registeredHandlers[i].eventType && this.registeredHandlers[i].handler){
        			canvas.removeEventListener(this.registeredHandlers[i].eventType, this.registeredHandlers[i].handler);
        		}
        	}
        	
        	this.registeredHandlers = [];
        };
        
        /**
         * Activate all default unit movement handlers
         * 
         * @this Battlemap.Cursor
         * @function Battlemap.Cursor#readyMoveHandlers
         */
        Cursor.prototype.readyMoveHandlers = function(){
        	if(theAI.enabled){
        		return;
        	}
        	this.registeredHandlers.push({eventType: 'keydown', handler: this.unitKeyMove});
        	canvas.addEventListener(this.registeredHandlers[this.registeredHandlers.length - 1].eventType, 
        							this.registeredHandlers[this.registeredHandlers.length - 1].handler);
        };
        
        /**
    	 * Add handlers for responding to mouse events
    	 * 
    	 * @this Battlemap.Cursor
    	 * @function Battlemap.Cursor#addMouseHandlers
    	 */
        Cursor.prototype.addMouseHandlers = function(){
        	/*
        	 * MouseEvent listener for the move command
        	 */
        	canvas.addEventListener('click', function(event){
        		if(Units.activeToken.moving){
        			return; // Ignore clicks if the token is already moving
        		}
        		var scrollOffsets = getScrollOffsets();
        		var x = event.clientX - canvasContainer.offsetLeft + scrollOffsets.x;
            	var y = event.clientY - canvasContainer.offsetTop + scrollOffsets.y;
            	
            	var gridX = Math.floor((x - gridXOffset) / tileWidth);
            	var gridY = Math.floor((y - gridYOffset) / tileHeight);
            	
            	for(var index in Units.activeToken.indexesInRange){
            		var tempGridX = Units.activeToken.indexesInRange[index] % gridWidth;
                    var tempGridY = Math.floor(Units.activeToken.indexesInRange[index] / gridHeight);
                    
                    if(tempGridX === gridX && tempGridY === gridY){
                    	// User is clicking a tile that the token can move to
                    	
                    	// Find the token's current tile coordinates
                    	var currentX = Units.activeToken.dataIndex % gridWidth;
                    	var currentY = Math.floor(Units.activeToken.dataIndex / gridHeight);
                    	
                    	var xDiff = gridX - currentX;
                    	var yDiff = gridY - currentY;
                    	
                    	var xDataIndex = Units.activeToken.dataIndex + xDiff;
                    	var yDataIndex = xDataIndex + (gridHeight * yDiff);
                    	
                    	Units.activeToken.yDataIndex = yDataIndex;
                    	Units.activeToken.yDiff = yDiff;
                    	
                    	if(xDiff === 0){
                    		Units.activeToken.movePiece(yDataIndex, xDiff, yDiff);
                    	}else {
                    		Units.activeToken.movePiece(xDataIndex, xDiff, yDiff);
                    	}
                    	break; // Exit the loop
                    }
            	}
        	});
        };
        
        /**
         * Moves cursor to the tile in the specified direction, if that
         * tile can be moved to.
         * 
         * @this Battlemap.Cursor
         * @function Battlemap.Cursor#move
         * @param {string} direction
         */
        Cursor.prototype.move = function(direction){
        	var newDataIndex;
        	if(canMovePiece(this.dataIndex, direction)){
        		switch(direction){
        			case 'Up':
        				newDataIndex = this.dataIndex - gridHeight;
        			break;
        			case 'Left':
        				newDataIndex = this.dataIndex - 1;
            		break;
        			case 'Down':
        				newDataIndex = this.dataIndex + gridHeight;
            		break;
        			case 'Right':
        				newDataIndex = this.dataIndex + 1;
            		break;
        		}
        		
        		if(activeMenu.activeCommand){
         		   if(activeMenu.activeCommand === 'Move'){
         			   var ranges = Units.activeToken.indexesInRange;
         			   var inRange = false;
         			   for(var i in ranges){
         				   if(newDataIndex === ranges[i]){
         					   inRange = true;
         					   break;
         				   }
         			   }
         			   if(!inRange){
         				   return false;
         			   }
         		   }
         	   }
        		
        		this.dataIndex = newDataIndex;
        		
        		this.draw(newDataIndex);
        	} else {
        		return false;
        	}
        };
        
        /**
         * Set and draw the cursor's position
         * 
         * @param {number} dataIndex The data index where the cursor will be drawn
         * @this Battlemap.Cursor
         * @function Battlemap.Cursor#setPosition
         */
        Cursor.prototype.setPosition = function(dataIndex){
        	if(dataIndex > 0 && dataIndex < currentUnits.data.length){
        		this.dataIndex = dataIndex;
        		
        		this.draw(dataIndex);
        	}
        };
        
        /**
         * Draws the cursor at the new position.
         * @this Battlemap.Cursor
         * @function Battlemap.Cursor#draw
         * @param {number} newDataIndex The new dataIndex where the cursor will be drawn
         */
        Cursor.prototype.draw = function(newDataIndex){
        	if(newDataIndex == null){
        		newDataIndex = gridWidth * (gridHeight - 1);
        	}
        	/* 
            Find out the original grid coordinates of the unit before
            it was moved
            */
            var curGridX = this.dataIndex % gridWidth;
            var curGridY = Math.floor(this.dataIndex / gridHeight);
            
            /*
            Determine the initial pixel coordinates of the piece before moving
            */
            var curPosX = curGridX*tileWidth + gridXOffset;
            var curPosY = curGridY*tileHeight + gridYOffset;
            
            /*
            Find out the new grid coordinates after the move
            */
            var newGridX = newDataIndex % gridWidth;
            var newGridY = Math.floor(newDataIndex / gridHeight);
            
            /*
            Determine the new pixel coordinates of the piece after moving
            */
            var newPosX = newGridX*tileWidth + gridXOffset;
            var newPosY = newGridY*tileHeight + gridYOffset;        	
            
            ctx.save();
            
            ctx.strokeStyle = 'rgba(255,0,0,0.4)';
            ctx.lineWidth = 3;
            
            background.redraw();
            units.draw();
            ctx.strokeRect(newPosX, newPosY, tileWidth, tileHeight);
            cursor.dataIndex = newDataIndex;
            
            ctx.restore();
        };
        
        /**
         * Remove all event listeners from the canvas that are also referenced
         * in the handlers parameter.
         * 
         * @function Battlemap~removeHandlers
         * @param {array} handlers An array of objects with property eventType
         * 						   that names the type of event and property
         * 						   handler that is the function to be removed
         */
        function removeHandlers(handlers){
        	for(var i in handlers){
        		if(handlers[i].eventType && handlers[i].handler){
        			canvas.removeEventListener(handlers[i].eventType, handlers[i].handler);
        		}
        	}
        }
        
        // Return the current scrollbar offsets as the x and y properties of an object
    	// David Flanagan, JavaScript: The Definitive Guide, 6th Edition 2011
    	function getScrollOffsets(w) {
    	    // Use the specified window or the current window if no argument
    	    w = w || window;

    	    // This works for all browsers except IE versions 8 and before
    	    if (w.pageXOffset != null) return {x: w.pageXOffset, y:w.pageYOffset};

    	    // For IE (or any browser) in Standards mode
    	    var d = w.document;
    	    if (document.compatMode == "CSS1Compat")
    	        return {x:d.documentElement.scrollLeft, y:d.documentElement.scrollTop};

    	    // For browsers in Quirks mode
    	    return { x: d.body.scrollLeft, y: d.body.scrollTop };
    	}
        
    	/**
    	 * Determines whether or not something at the specified
    	 * data index can move the specified direction.
    	 * 
    	 * @function Battlemap~canMovePiece
    	 * @param {number} dataIndex The current location of the token or cursor
    	 * @param {string} direction The direction the user wishes to move
    	 */
    	function canMovePiece(dataIndex, direction, newDataIndex){
    		if(newDataIndex){
     		   switch(newDataIndex){    		   
 	    		   case dataIndex - gridWidth:
 	    			   direction = 'Up';
 	    		   break;
 	    		   case dataIndex - 1:
 	    			   direction = 'Left';
 	    		   break;
 	    		   case dataIndex + gridWidth:
 	    		   		direction = 'Down';
 	    		   case dataIndex + 1:
 	    			   direction = 'Right';
     		   }     	   	
     	   }
    		
     	   switch(direction){
 	           case 'Up':
 	               if(dataIndex < gridHeight){
 	                   // We're on the top row, so we can't move up.
 	                   return false;
 	               }
 	           break;
 	           case 'Left':
 	               if(dataIndex % gridWidth === 0){
 	                   // We're in the left most column, so we can't move left.
 	                   return false;
 	               }
 	           break;
 	           case 'Down':
 	               if((dataIndex + gridHeight) >= background.data.length){
 	                   // We're on the bottom row, so we can't move down.
 	                   return false;
 	               }
 	           break;
 	           case 'Right':
 	               if((dataIndex % gridWidth) === (gridWidth - 1)){
 	                   // We're in the right most column, so we can't move right.
 	                   return false;
 	               }
 	           break;
 	           default:
 	        	   return false;
 	           break;
     	   }
     	   
     	   return true;
        }
    	
        /**
         * Convert a data index in the grid to grid and document coordinates
         * @function Battlemap~indexToCoordinates
         * @param {number} dataIndex The data index to be converted to an
         * 							 object of x and y grid and pixel coordinates
         * @return {object}
         */
        function indexToCoordinates(dataIndex){
        	/* 
            Find out the original grid coordinates of the unit before
            it was moved
            */
            var gridX = dataIndex % gridWidth;
            var gridY = Math.floor(dataIndex / gridHeight);
            
            /*
            Determine the document coordinates based on the known
            position of the canvas as well as the token's data index
            */
            var posX = gridX*tileWidth + gridXOffset;
            var posY = gridY*tileHeight + gridYOffset;
            
            return {
            	gridX: gridX,
            	gridY: gridY,
            	posX: posX,
            	posY: posY
            };
        }
        
        /**
        Sets up the key bindings for the specified player
        @function Battlemap~registerKeys
        @param {string} player The player for whom keys are being registered. The default is "player"
        */
        function registerKeys(player){
        	var tileIndex = document.querySelector('.indexes .tile');
        	
            if(!player){
                player = "player";
            }
            
            /*
             * Show information about the canvas and mouse coordinates
             * as well as movement availability.
             */
            canvas.addEventListener('mousemove', function(event){
            	var scrollOffsets = getScrollOffsets();
            	var x = event.clientX - document.querySelector('#canvasContainer').offsetLeft + scrollOffsets.x;
            	var y = event.clientY - document.querySelector('#canvasContainer').offsetTop + scrollOffsets.y;
            	document.querySelector('.position').innerHTML = x + ', ' + y;
            	
            	var gridX = Math.floor((x - gridXOffset) / tileWidth);
            	var gridY = Math.floor((y - gridYOffset) / tileHeight);
            	
            	if((gridX >= 0 && gridX < gridWidth) && (gridY >= 0) && gridY < gridHeight){
            		document.querySelector('.coordinates .tile').innerHTML = gridX + ', ' + gridY;
                	
                	var currentIndex = gridY * gridWidth + gridX;
                	tileIndex.innerHTML = currentIndex;
                	
                	for(var index in Units.activeToken.indexesInRange){
                		var tempGridX = Units.activeToken.indexesInRange[index] % gridWidth;
                        var tempGridY = Math.floor(Units.activeToken.indexesInRange[index] / gridHeight);
                        
                        if(tempGridX === gridX && tempGridY === gridY){
                        	console.log("you can move here");
                        	break;
                        }
                	}
            	}
            	
            });
            
            /*
             * Handlers for loading maps
             */
            
            var maploadButton = document.querySelector('#filelistcontainer #Load');
            maploadButton.addEventListener('click', background.load);
            
            // User can also create a new randomly generated map
            var maprandomButton = document.querySelector('#filelistcontainer #Random');            
            maprandomButton.addEventListener('click', function(event){
            	window.localStorage.removeItem('background');
            	window.location = window.location;
            });
            
        }
    }   
});

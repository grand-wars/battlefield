﻿<?php 
session_start();
$_SESSION['load'] = true;

// Test the expected locations of the maps directory
$prefix = $_SERVER['REQUEST_SCHEME']. '://' . $_SERVER['SERVER_NAME'] . '/game';
$maplocation =  $prefix . '/mapeditor/maps/location.php';

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Battlefield Map</title>
<link href="css/index.css" type="text/css" rel="stylesheet" />
<script>
window.onload = function(){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '<?php echo $maplocation; ?>');
	xhr.responseType = 'json';
	
	xhr.addEventListener('load', function(event){
		var select = document.querySelector('#filelist');
		var mapList = this.response;

		for(var mapIndex in mapList){
			var option = document.createElement('option');
			var map = mapList[mapIndex];
			option.textContent = mapList[mapIndex];
			option.value = mapList[mapIndex];
			select.appendChild(option);
		}
	});

	xhr.addEventListener('error', function(event){
		var messages = document.querySelector('.messages');

		messages.innerHTML = '<p class="warning">Map directory could not be loaded</p>';
	});

	xhr.send();
}
</script>
<script type="application/javascript" src="scripts/main.js">
</script>

</head>

<body>
    <noscript>Please enable JavaScript to play.</noscript>
    <div id="canvasContainer">
	    <canvas id="mainscreen" width="700" height="700" tabindex="1"><!-- Let there be focus! -->
	    Main Screen
	    </canvas>
	    <div id="cursorInfo" class="closed">
	    	<p class="background"></p>
    	</div>
    	<div id="unitInfo" class="closed">
    	</div>
    	<div id="menu">
			<ul class="closed">
			</ul>
		</div>
		<div id="days" class="inactive">
			<h1></h1>
		</div>
    </div>
	<div id="sidebox">
        <h1 class="description">Test Description</h1>
        <h2 class="type">Test type</h2>
        <hgroup class="coordinates">
	        <h2 class="position"></h2>
	        <h2 class="tile"></h2>
        </hgroup>
        <hgroup class="indexes">
        	<h2 class="tile"></h2>
        </hgroup>
        <label for="mute">Mute</label>
		<input type="checkbox" id="mute">
		
		<div id="filelistcontainer">
			<h1>Maps:</h1>
			<select id="filelist">
<?php

?>
			</select>
			<button id="Load" type="submit">Load</button>
			<button id="Random" type="submit">Random</button>
			<div class="messages"></div>
		</div>
		
    </div>
    
</body>
</html>

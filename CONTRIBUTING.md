#Committing
If developing a new feature of significance that you would like other developers to contribute to, create a new branch in your local repository named after the feature 
and then push it to the `origin`, which should be the remote for the [Gitlab repository] [gitlab]. Once the feature is ready, the lead for the project will merge it into `dev`. 
When everything is confirmed to be in working order, those changes will be merged into `master`, which should be the project's final state. The feature branch will then 
be deleted on the server, and you should remove it from your local repository. There should not be any occasion to commit directy to `master`.

#Documentation
The JavaScript APIs created in the project are documented with [JSDoc 3] [jsdoc3], so confer with their usage guide for how to make your comments
show up in the generated documentation. Just remember, too much is better than too little information.

[jsdoc3]: http://usejsdoc.org/
[gitlab]: ssh://git@gitlab.com:grand-wars/battlefield.git